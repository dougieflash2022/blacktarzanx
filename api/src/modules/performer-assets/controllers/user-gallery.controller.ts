import {
  Controller,
  Injectable,
  HttpCode,
  HttpStatus,
  UsePipes,
  ValidationPipe,
  Param,
  Get,
  Query
} from '@nestjs/common';
import { DataResponse } from 'src/kernel';
import { GallerySearchRequest } from '../payloads';
import { GalleryService } from '../services/gallery.service';

@Injectable()
@Controller('user/performer-assets/galleries')
export class UserGalleryController {
  constructor(private readonly galleryService: GalleryService) {}

  @Get('/search')
  @HttpCode(HttpStatus.OK)
  @UsePipes(new ValidationPipe({ transform: true }))
  async searchGallery(
    @Query() req: GallerySearchRequest
  ): Promise<any> {
    const resp = await this.galleryService.userSearch(req);
    return DataResponse.ok(resp);
  }

  @Get('/:id/view')
  @HttpCode(HttpStatus.OK)
  @UsePipes(new ValidationPipe({ transform: true }))
  async view(@Param('id') id: string): Promise<any> {
    const resp = await this.galleryService.details(id);
    return DataResponse.ok(resp);
  }
}
