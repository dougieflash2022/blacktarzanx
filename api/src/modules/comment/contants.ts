export const OBJECT_TYPE = {
  VIDEO: 'video',
  PERFORMER: 'performer'
};

export const COMMENT_CHANNEL = 'COMMENT_CHANNEL';
