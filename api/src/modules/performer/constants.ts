export const PERFORMER_STATUSES = {
  ACTIVE: 'active',
  INACTIVE: 'inactive',
  PENDING: 'pending-email-confirmation'
};

export const PERFORMER_UPDATE_STATUS_CHANNEL = 'PERFORMER_UPDATE_STATUS_CHANNEL';
