import {
  Injectable,
  Inject,
  forwardRef,
  BadRequestException
} from '@nestjs/common';
import { PerformerService } from 'src/modules/performer/services';
import {
  QueueEventService,
  QueueEvent
} from 'src/kernel';
import { EVENT } from 'src/kernel/constants';
import { Model } from 'mongoose';
import { ObjectId } from 'mongodb';
import { PAYMENT_TRANSACTION_MODEL_PROVIDER } from '../providers';
import { OrderModel, PaymentTransactionModel } from '../models';
import {
  PAYMENT_STATUS,
  PAYMENT_TYPE,
  TRANSACTION_SUCCESS_CHANNEL
} from '../constants';
import { SubscriptionService } from '../../subscription/services/subscription.service';
import { CCBillService } from './ccbill.service';
import { OrderService } from './order.service';
import { MissingConfigPaymentException } from '../exceptions';

@Injectable()
export class PaymentService {
  constructor(
    @Inject(forwardRef(() => PerformerService))
    private readonly performerService: PerformerService,
    @Inject(PAYMENT_TRANSACTION_MODEL_PROVIDER)
    private readonly paymentTransactionModel: Model<PaymentTransactionModel>,
    private readonly ccbillService: CCBillService,
    private readonly queueEventService: QueueEventService,
    private readonly subscriptionService: SubscriptionService,
    private readonly orderService: OrderService
  ) { }

  public async findById(id: string | ObjectId) {
    return this.paymentTransactionModel.findById(id);
  }

  private async getPerformerSinglePaymentGatewaySetting(performerId, paymentGateway = 'ccbill') {
    // get performer information and do transaction
    const performerPaymentSetting = await this.performerService.getPaymentSetting(
      performerId,
      paymentGateway
    );
    if (!performerPaymentSetting) {
      throw new MissingConfigPaymentException();
    }

    // apply for only ccbill right now
    const flexformId = performerPaymentSetting?.value?.flexformId;
    const subAccountNumber = performerPaymentSetting?.value?.singlePurchaseSubAccountNumber;
    const salt = performerPaymentSetting?.value?.salt;

    if (!flexformId || !subAccountNumber || !salt) {
      throw new MissingConfigPaymentException();
    }

    return {
      flexformId,
      subAccountNumber,
      salt
    };
  }

  private async getPerformerSubscroptionPaymentGatewaySetting(performerId, paymentGateway = 'ccbill') {
    // get performer information and do transaction
    const performerPaymentSetting = await this.performerService.getPaymentSetting(
      performerId,
      paymentGateway
    );
    if (!performerPaymentSetting) {
      throw new MissingConfigPaymentException();
    }

    // apply for only ccbill right now
    const flexformId = performerPaymentSetting?.value?.flexformId;
    const subAccountNumber = performerPaymentSetting?.value?.subscriptionSubAccountNumber;
    const salt = performerPaymentSetting?.value?.salt;

    if (!flexformId || !subAccountNumber || !salt) {
      throw new MissingConfigPaymentException();
    }

    return {
      flexformId,
      subAccountNumber,
      salt
    };
  }

  public async subscribePerformer(order: OrderModel, paymentGateway = 'ccbill') {
    const {
      flexformId,
      subAccountNumber,
      salt
    } = await this.getPerformerSubscroptionPaymentGatewaySetting(order.sellerId);

    const transaction = await this.paymentTransactionModel.create({
      paymentGateway,
      orderId: order._id,
      source: order.buyerSource,
      sourceId: order.buyerId,
      type: order.type,
      totalPrice: order.totalPrice,
      products: [],
      status: PAYMENT_STATUS.PENDING
    });
    return this.ccbillService.subscription({
      salt,
      flexformId,
      subAccountNumber,
      price: parseFloat(order.totalPrice.toFixed(2)),
      transactionId: transaction._id,
      subscriptionType: order.type
    });
  }

  public async purchasePerformerProducts(order: OrderModel, paymentGateway = 'ccbill') {
    const {
      flexformId,
      subAccountNumber,
      salt
    } = await this.getPerformerSinglePaymentGatewaySetting(order.sellerId);

    const transaction = await this.paymentTransactionModel.create({
      paymentGateway,
      orderId: order._id,
      source: order.buyerSource,
      sourceId: order.buyerId,
      type: PAYMENT_TYPE.PERFORMER_PRODUCT,
      totalPrice: order.totalPrice,
      status: PAYMENT_STATUS.PENDING,
      products: []
    });
    return this.ccbillService.singlePurchase({
      salt,
      flexformId,
      subAccountNumber,
      price: order.totalPrice,
      transactionId: transaction._id
    });
  }

  public async purchasePerformerVOD(order: OrderModel, paymentGateway = 'ccbill') {
    const {
      flexformId,
      subAccountNumber,
      salt
    } = await this.getPerformerSinglePaymentGatewaySetting(order.sellerId);

    const transaction = await this.paymentTransactionModel.create({
      paymentGateway,
      orderId: order._id,
      source: order.buyerSource,
      sourceId: order.buyerId,
      type: PAYMENT_TYPE.SALE_VIDEO,
      totalPrice: order.totalPrice,
      status: PAYMENT_STATUS.PENDING,
      products: []
    });
    return this.ccbillService.singlePurchase({
      salt,
      flexformId,
      subAccountNumber,
      price: order.totalPrice,
      transactionId: transaction._id
    });
  }

  public async ccbillSinglePaymentSuccessWebhook(payload: Record<string, any>) {
    const transactionId = payload['X-transactionId'] || payload.transactionId;
    if (!transactionId) {
      throw new BadRequestException();
    }
    const checkForHexRegExp = new RegExp('^[0-9a-fA-F]{24}$');
    if (!checkForHexRegExp.test(transactionId)) {
      return { ok: false };
    }
    const transaction = await this.paymentTransactionModel.findById(
      transactionId
    );
    if (!transaction || transaction.status !== PAYMENT_STATUS.PENDING) {
      return { ok: false };
    }
    transaction.status = PAYMENT_STATUS.SUCCESS;
    transaction.paymentResponseInfo = payload;
    transaction.updatedAt = new Date();
    await transaction.save();
    await this.queueEventService.publish(
      new QueueEvent({
        channel: TRANSACTION_SUCCESS_CHANNEL,
        eventName: EVENT.CREATED,
        data: transaction
      })
    );
    return { ok: true };
  }

  public async ccbillRenewalSuccessWebhook(payload: any) {
    const subscriptionId = payload.subscriptionId || payload.subscription_id;
    if (!subscriptionId) {
      throw new BadRequestException();
    }

    const subscription = await this.subscriptionService.findBySubscriptionId(subscriptionId);
    if (!subscription) {
      // TODO - should check in case admin delete subscription??
      // TODO - log me
      return { ok: false };
    }

    // create user order and transaction for this order
    const price = payload.billedAmount || payload.accountingAmount;
    const { userId } = subscription;
    const { performerId } = subscription;
    const order = await this.orderService.createForPerformerSubscriptionRenewal({
      userId,
      performerId,
      price,
      type: subscription.subscriptionType
    });

    const transaction = await this.paymentTransactionModel.create({
      paymentGateway: 'ccbill',
      orderId: order._id,
      source: order.buyerSource,
      sourceId: order.buyerId,
      type: order.type,
      totalPrice: order.totalPrice,
      status: PAYMENT_STATUS.SUCCESS,
      paymentResponseInfo: payload,
      products: []
    });

    await this.queueEventService.publish(
      new QueueEvent({
        channel: TRANSACTION_SUCCESS_CHANNEL,
        eventName: EVENT.CREATED,
        data: transaction
      })
    );
    return { ok: true };
  }
}
