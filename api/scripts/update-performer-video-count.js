const { DB, COLLECTION } = require('../migrations/lib');

module.exports = async () => {
  const performers = await DB.collection(COLLECTION.PERFORMER).find().toArray();
  // eslint-disable-next-line no-restricted-syntax
  for (const performer of performers) {
    // eslint-disable-next-line no-await-in-loop
    const videoCount = await DB.collection(COLLECTION.PERFORMER_VIDEO).count({ performerId: performer._id });
    // eslint-disable-next-line no-await-in-loop
    await DB.collection(COLLECTION.PERFORMER).updateOne({ _id: performer._id }, {
      $set: {
        'stats.totalVideos': videoCount
      }
    });
  }
};
