import Head from 'next/head';
import { PureComponent, Fragment, createRef } from 'react';
import { Tabs, message } from 'antd';
import Page from '@components/common/layout/page';
import { AccountForm } from '@components/performer/AccountForm';
import { PerformerDocument } from '@components/performer/Document';
import { SubscriptionForm } from '@components/performer/Subcription';
import { BankingForm } from '@components/performer/BankingForm';
import { CCbillSettingForm } from '@components/performer/ccbill-setting';
import { CommissionSettingForm } from '@components/performer/commission-setting';
import {
  ICountry,
  ILangguges,
  IPhoneCodes,
  IPerformerUpdate,
  IPerformerCategory
} from 'src/interfaces';
import { authService, performerService, performerCategoryService } from '@services/index';
import Loader from '@components/common/base/loader';
import { utilsService } from '@services/utils.service';
import { UpdatePaswordForm } from '@components/user/update-password-form';
import { BreadcrumbComponent } from '@components/common';
import { omit } from 'lodash';

interface IProps {
  id: string;
  countries: ICountry[];
  languages: ILangguges[];
  phoneCodes: IPhoneCodes[];
  categories: IPerformerCategory[];
}
class PerformerUpdate extends PureComponent<IProps> {
  static async getInitialProps({ ctx }) {
    const [countries, languages, phoneCodes, categories] = await Promise.all([
      utilsService.countriesList(),
      utilsService.languagesList(),
      utilsService.phoneCodesList(),
      performerCategoryService.search({
        sortBy: 'ordering',
        sort: 'asc',
        limit: 100
      })
    ]);
    return {
      countries: countries.data,
      languages: languages.data,
      phoneCodes: phoneCodes.data,
      categories: categories.data && categories.data.data ? categories.data.data : [],
      ...ctx.query
    };
  }

  formRef = createRef() as any;

  state = {
    pwUpdating: false,
    updating: false,
    fetching: false,
    performer: {} as IPerformerUpdate,
    settingUpdating: false
  };

  customFields = {};

  async componentDidMount() {
    const { id } = this.props;
    try {
      this.setState({ fetching: true });
      const resp = await performerService.findById(id);
      this.setState({ performer: resp.data });
      // if (resp.data && resp.data.schedule) {
      //   this.scheduleValue = { ...this.scheduleValue, ...resp.data.schedule };
      // }
    } catch (e) {
      message.error('Error while fecting performer!');
    } finally {
      this.setState({ fetching: false });
    }
  }

  onFormRefSubmit() {
    this.formRef && this.formRef.formRefSubmit();
  }

  onUploaded(field: string, resp: any) {
    this.customFields[field] = resp.response.data._id;
  }

  async updatePassword(data: any) {
    const { id } = this.props;
    try {
      this.setState({ pwUpdating: true });
      await authService.updatePassword(data.password, id, 'performer');
      message.success('Password has been updated!');
    } catch (e) {
      message.error('An error occurred, please try again!');
    } finally {
      this.setState({ pwUpdating: false });
    }
  }

  async updatePaymentGatewaySetting(data: any) {
    const { id } = this.props;
    try {
      this.setState({ settingUpdating: true });
      await performerService.updatePaymentGatewaySetting(id, {
        performerId: id,
        key: 'ccbill',
        status: 'active',
        value: data
      });
      message.success('CCbill setting has been updated!');
    } catch (error) {
      message.error('An error occurred, please try again!');
    } finally {
      this.setState({ settingUpdating: false });
    }
  }

  async updateCommissionSetting(data: any) {
    const { id } = this.props;
    if (
      data.monthlySubscriptionCommission < 0
      || data.monthlySubscriptionCommission > 1
      || data.yearlySubscriptionCommission < 0
      || data.yearlySubscriptionCommission > 1
      || data.videoSaleCommission < 0
      || data.videoSaleCommission > 1
      || data.productSaleCommission < 0
      || data.productSaleCommission > 1
    ) {
      return message.error('Comission must be greater than 0 or smaller than 1!');
    }

    try {
      this.setState({ settingUpdating: true });
      await performerService.updateCommissionSetting(id, { ...data, performerId: id });
      message.success('Commission setting has been updated!');
    } catch (error) {
      message.error('An error occurred, please try again!');
    } finally {
      this.setState({ settingUpdating: false });
    }
    return undefined;
  }

  async updateBankingSetting(data: any) {
    const { id } = this.props;
    try {
      this.setState({ settingUpdating: true });
      await performerService.updateBankingSetting(id, { ...data, performerId: id });
      message.success('Banking setting has been updated!');
    } catch (error) {
      message.error('An error occurred, please try again!');
    } finally {
      this.setState({ settingUpdating: false });
    }
  }

  async submit(data: any) {
    const { id } = this.props;
    let newData = data;
    try {
      if (data.status === 'pending-email-confirmation') {
        newData = omit(data, ['status']);
      }
      this.setState({ updating: true });
      const updated = await performerService.update(id, {
        ...newData,
        ...this.customFields
      });
      this.setState({ performer: updated.data });
      message.success('Updated successfully');
    } catch (e) {
      // TODO - exact error message
      const error = await e;
      message.error(error && (error.message || 'An error occurred, please try again!'));
    } finally {
      this.setState({ updating: false });
    }
  }

  render() {
    const {
      pwUpdating, performer, updating, fetching, settingUpdating
    } = this.state;
    const {
      countries, languages
    } = this.props;
    return (
      <>
        <Head>
          <title>Performer update</title>
        </Head>
        <BreadcrumbComponent
          breadcrumbs={[
            { title: 'Performers', href: '/performer' },
            { title: performer.username },
            { title: 'Update' }
          ]}
        />
        <Page>
          {fetching ? (
            <Loader />
          ) : (
            <Tabs defaultActiveKey="basic" tabPosition="left">
              <Tabs.TabPane tab={<span>General info</span>} key="basic">
                <AccountForm
                  onUploaded={this.onUploaded.bind(this)}
                  onFinish={this.submit.bind(this)}
                  performer={performer}
                  submiting={updating}
                  countries={countries}
                  languages={languages}
                  // phoneCodes={phoneCodes}
                  // categories={categories}
                />
              </Tabs.TabPane>
              <Tabs.TabPane tab={<span>Subscription Setting</span>} key="subscription">
                <SubscriptionForm
                  submiting={updating}
                  onFinish={this.submit.bind(this)}
                  performer={performer}
                />
              </Tabs.TabPane>
              <Tabs.TabPane tab={<span>Banking Setting</span>} key="banking">
                <BankingForm
                  submiting={settingUpdating}
                  onFinish={this.updateBankingSetting.bind(this)}
                  bankingInformation={performer.bankingInformation || null}
                  countries={countries}
                />
              </Tabs.TabPane>
              <Tabs.TabPane tab={<span>CCbill Setting</span>} key="ccbill">
                <CCbillSettingForm
                  submiting={settingUpdating}
                  onFinish={this.updatePaymentGatewaySetting.bind(this)}
                  ccbillSetting={performer.ccbillSetting}
                />
              </Tabs.TabPane>
              <Tabs.TabPane tab={<span>Commission Setting</span>} key="commission">
                <CommissionSettingForm
                  submiting={settingUpdating}
                  onFinish={this.updateCommissionSetting.bind(this)}
                  commissionSetting={performer.commissionSetting}
                />
              </Tabs.TabPane>
              <Tabs.TabPane tab={<span>Document</span>} key="document">
                <PerformerDocument
                  submiting={updating}
                  onUploaded={this.onUploaded.bind(this)}
                  onFinish={this.submit.bind(this)}
                  performer={performer}
                />
              </Tabs.TabPane>
              <Tabs.TabPane tab={<span>Change password</span>} key="password">
                <UpdatePaswordForm onFinish={this.updatePassword.bind(this)} updating={pwUpdating} />
              </Tabs.TabPane>
            </Tabs>
          )}
        </Page>
      </>
    );
  }
}

export default PerformerUpdate;
