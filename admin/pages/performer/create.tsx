import Head from 'next/head';
import { PureComponent, Fragment, createRef } from 'react';
import { message, Tabs } from 'antd';
import Page from '@components/common/layout/page';
import {
  ICountry, ILangguges, IPerformerCategory
} from 'src/interfaces';
import Router from 'next/router';
import { performerService } from '@services/index';
import { utilsService } from '@services/utils.service';
import { validateUsername, getResponseError } from '@lib/utils';
import { AccountForm } from '@components/performer/AccountForm';
// import { PerformerDocument } from '@components/performer/Document';
import { BreadcrumbComponent } from '@components/common';

interface IProps {
  countries: ICountry[];
  languages: ILangguges[];
  categories: IPerformerCategory[];
}
class PerformerCreate extends PureComponent<IProps> {
  static async getInitialProps() {
    const [countries, languages] = await Promise.all([
      utilsService.countriesList(),
      utilsService.languagesList()
      // performerCategoryService.search({
      //   sortBy: 'ordering',
      //   sort: 'asc',
      //   limit: 100
      // })
    ]);
    return {
      countries: countries.data,
      languages: languages.data
      // categories: categories.data && categories.data.data ? categories.data.data : []
    };
  }

  state = {
    creating: false
  };

  customFields = {};

  formRef = createRef() as any;

  onUploaded(field: string, resp: any) {
    this.customFields[field] = resp.response.data._id;
  }

  async submit(data: any) {
    try {
      if (data.password !== data.rePassword) {
        return message.error('Confirm password mismatch!');
      }

      if (!validateUsername(data.username)) {
        return message.error('Username must contain only Alphabets & Numbers');
      }

      this.setState({ creating: true });
      const resp = await performerService.create({
        ...data,
        ...this.customFields
        // schedule: this.scheduleValue
      });
      message.success('Created successfully');
      Router.push(
        {
          pathname: '/performer',
          query: { id: resp.data._id }
        },
        '/performer',
        {
          shallow: false
        }
      );
    } catch (e) {
      const err = (await Promise.resolve(e)) || {};
      message.error(getResponseError(err) || 'An error occurred, please try again!');
    } finally {
      this.setState({ creating: false });
    }
    return undefined;
  }

  render() {
    const { creating } = this.state;
    const { countries, languages, categories } = this.props;

    return (
      <>
        <Head>
          <title>Create performer</title>
        </Head>
        <BreadcrumbComponent
          breadcrumbs={[{ title: 'Performers', href: '/performer' }, { title: 'Create new performer' }]}
        />
        <Page>
          <Tabs defaultActiveKey="basic" tabPosition="left">
            <Tabs.TabPane tab={<span>General info</span>} key="basic">
              <AccountForm
                ref={this.formRef}
                onUploaded={this.onUploaded.bind(this)}
                onFinish={this.submit.bind(this)}
                submiting={creating}
                countries={countries}
                languages={languages}
                categories={categories}
              />
            </Tabs.TabPane>
          </Tabs>
        </Page>
      </>
    );
  }
}

export default PerformerCreate;
