import Head from 'next/head';

const Home = () => (
  <div className="container">
    <Head>
      <title>Admin Panel</title>
      <link rel="icon" href="/favicon.ico" />
    </Head>

    <main>
      Loading
    </main>

  </div>
);

export default Home;
