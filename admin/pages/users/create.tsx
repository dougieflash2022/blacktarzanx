import Head from 'next/head';
import { PureComponent, Fragment } from 'react';
import { message } from 'antd';
import Page from '@components/common/layout/page';
import { ICountry } from 'src/interfaces';
import Router from 'next/router';
import { userService } from '@services/index';
import { utilsService } from '@services/utils.service';
import { validateUsername, getResponseError } from '@lib/utils';
import { AccountForm } from '@components/user/account-form';

// const layout = {
//   labelCol: { span: 4 },
//   wrapperCol: { span: 16 }
// };

// const validateMessages = {
//   required: 'This field is required!',
//   types: {
//     email: 'Not a validate email!',
//     number: 'Not a validate number!'
//   },
//   number: {
//     range: 'Must be between ${min} and ${max}'
//   }
// };

interface IProps {
  countries: ICountry[];
}
class UserCreate extends PureComponent<IProps> {
  static async getInitialProps() {
    const resp = await utilsService.countriesList();
    return {
      countries: resp.data
    };
  }

  state = {
    // pwUpdating: false,
    creating: false
    // fetching: false,
    //  user: {} as IUser
  };

  _avatar: File;

  onBeforeUpload(file) {
    this._avatar = file;
  }

  async submit(data: any) {
    try {
      if (data.password !== data.rePassword) {
        return message.error('Confirm password mismatch!');
      }

      if (!validateUsername(data.username)) {
        return message.error('Username must contain only Alphabets & Numbers');
      }

      this.setState({ creating: true });
      const resp = await userService.create(data);
      message.success('Updated successfully');
      if (this._avatar) {
        await userService.uploadAvatarUser(this._avatar, resp.data._id);
      }
      Router.push(
        {
          pathname: '/users'
        },
        '/users'
      );
    } catch (e) {
      const err = (await Promise.resolve(e)) || {};
      message.error(getResponseError(err) || 'An error occurred, please try again!');
    } finally {
      this.setState({ creating: false });
    }
    return undefined;
  }

  render() {
    const { creating } = this.state;
    const { countries } = this.props;
    // const uploadHeaders = {
    //   authorization: authService.getToken()
    // };
    return (
      <>
        <Head>
          <title>Create user</title>
        </Head>
        <Page>
          <AccountForm
            onFinish={this.submit.bind(this)}
            updating={creating}
            options={{
              beforeUpload: this.onBeforeUpload.bind(this)
            }}
            countries={countries}
          />
        </Page>
      </>
    );
  }
}

export default UserCreate;
