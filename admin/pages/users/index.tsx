/* eslint-disable no-nested-ternary */
import Head from 'next/head';
import Link from 'next/link';
import { PureComponent, Fragment } from 'react';
import { connect } from 'react-redux';
import {
  Table, message, Dropdown, Button, Menu, Tag
} from 'antd';
import Page from '@components/common/layout/page';
import { IUser } from 'src/interfaces';
import { userService } from '@services/user.service';
import { SearchFilter } from '@components/user/search-filter';
import { DownOutlined, EditOutlined } from '@ant-design/icons';
import { formatDate } from '@lib/date';

interface IProps {
  currentUser: IUser;
}
class Users extends PureComponent<IProps> {
  state = {
    pagination: {} as any,
    searching: false,
    list: [],
    limit: 10,
    filter: {} as any,
    sortBy: 'updatedAt',
    sort: 'desc'
  };

  componentDidMount() {
    this.search();
  }

  async handleTableChange(pagination, filters, sorter) {
    const pager = { ...pagination };
    pager.current = pagination.current;
    await this.setState({
      pagination: pager,
      sortBy: sorter.field || '',
      sort: sorter.order ? (sorter.order === 'descend' ? 'desc' : 'asc') : ''
    });
    this.search(pager.current);
  }

  async handleFilter(filter) {
    await this.setState({ filter });
    this.search();
  }

  async search(page = 1) {
    const {
      limit, filter,
      sort,
      sortBy, pagination
    } = this.state;
    try {
      await this.setState({ searching: true });

      const resp = await userService.search({
        limit,
        offset: (page - 1) * limit,
        ...filter,
        sort,
        sortBy
      });
      await this.setState({
        searching: false,
        list: resp.data.data,
        pagination: {
          ...pagination,
          total: resp.data.total
        }
      });
    } catch (e) {
      message.error('An error occurred, please try again!');
      this.setState({ searching: false });
    }
  }

  render() {
    const { list, searching, pagination } = this.state;
    const columns = [
      {
        title: 'First name',
        dataIndex: 'firstName',
        sorter: true
      },
      {
        title: 'Last name',
        dataIndex: 'lastName',
        sorter: true
      },
      {
        title: 'Username',
        dataIndex: 'username',
        sorter: true
      },
      {
        title: 'Email',
        dataIndex: 'email',
        sorter: true
      },
      {
        title: 'Roles',
        dataIndex: 'roles',
        render(roles: any) {
          return roles.map((role) => {
            switch (role) {
              case 'user':
                return <Tag color="blue" key={role}>User</Tag>;
              case 'admin':
                return <Tag color="red" key={role}>Admin</Tag>;
              default: return <Tag color="default">{roles}</Tag>;
            }
          });
        }
      },
      {
        title: 'Gender',
        dataIndex: 'gender',
        render(gender) {
          return <Tag color="green">{gender || 'N/A'}</Tag>;
        }
      },
      {
        title: 'Status',
        dataIndex: 'status',
        render(status) {
          switch (status) {
            case 'active':
              return <Tag color="green">Active</Tag>;
            case 'inactive':
              return <Tag color="red">Inactive</Tag>;
            case 'pending-email-confirmation':
              return <Tag color="default">Pending</Tag>;
            default: return <Tag color="default">{status}</Tag>;
          }
        }
      },
      {
        title: 'Verified Email',
        dataIndex: 'verifiedEmail',
        render(status) {
          switch (status) {
            case true:
              return <Tag color="green">Y</Tag>;
            case false:
              return <Tag color="red">N</Tag>;
            default: return <Tag color="default">{status}</Tag>;
          }
        }
      },
      // {
      //   title: 'Amount spent',
      //   dataIndex: '_id',
      //   render() {
      //     return <span>Not implement yet!</span>;
      //   }
      // },
      // {
      //   title: 'Watching time',
      //   dataIndex: '_id',
      //   render() {
      //     return <span>Not implement yet!</span>;
      //   }
      // },
      {
        title: 'CreatedAt',
        dataIndex: 'createdAt',
        sorter: true,
        render(date: Date) {
          return <span>{formatDate(date)}</span>;
        }
      },
      {
        title: '#',
        dataIndex: '_id',
        fixed: 'right' as 'right',
        render(id: string) {
          return (
            <Dropdown
              overlay={(
                <Menu>
                  <Menu.Item key="edit">
                    <Link
                      href={{
                        pathname: '/users/update',
                        query: { id }
                      }}
                      as={`/users/update?id=${id}`}
                    >
                      <a>
                        <EditOutlined />
                        {' '}
                        Update
                      </a>
                    </Link>
                  </Menu.Item>
                </Menu>
              )}
            >
              <Button>
                Actions
                {' '}
                <DownOutlined />
              </Button>
            </Dropdown>
          );
        }
      }
    ];
    return (
      <>
        <Head>
          <title>Users</title>
        </Head>
        <Page>
          <SearchFilter onSubmit={this.handleFilter.bind(this)} />
          <div style={{ marginBottom: '20px' }} />
          <Table
            dataSource={list}
            columns={columns}
            rowKey="_id"
            loading={searching}
            pagination={pagination}
            onChange={this.handleTableChange.bind(this)}
            scroll={{ x: '150vw', y: '90vh' }}
          />
        </Page>
      </>
    );
  }
}

const mapStates = (state: any) => ({
  currentUser: state.user.current
});
export default connect(mapStates)(Users);
