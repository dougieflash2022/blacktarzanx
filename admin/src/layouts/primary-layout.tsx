import * as React from 'react';
import { Layout, Drawer, BackTop } from 'antd';
import { enquireScreen, unenquireScreen } from 'enquire-js';
import { connect } from 'react-redux';
import { updateUIValue, loadUIValue } from 'src/redux/ui/actions';
import Sider from '@components/common/layout/sider';
import { IUIConfig } from 'src/interfaces/ui-config';
import {
  PieChartOutlined,
  ContainerOutlined,
  UserOutlined,
  WomanOutlined,
  VideoCameraOutlined,
  CameraOutlined,
  FileImageOutlined,
  SkinOutlined,
  DollarOutlined,
  HeartOutlined,
  MenuOutlined,
  MailOutlined
} from '@ant-design/icons';
import Header from '@components/common/layout/header';
import { Router } from 'next/router';
import Loader from '@components/common/base/loader';

import './primary-layout.less';

interface DefaultProps extends IUIConfig {
  children: any;
  config: IUIConfig;
  updateUIValue: Function;
  loadUIValue: Function;
}

export async function getStaticProps() {
  return {
    props: {}
  };
}

class PrimaryLayout extends React.PureComponent<DefaultProps> {
  state = {
    isMobile: false,
    // security request for primary layout
    // checkingUser: false,
    routerChange: false
  };

  enquireHandler: any;

  componentDidMount() {
    const { loadUIValue: handleLoadUI } = this.props;
    handleLoadUI();
    this.enquireHandler = enquireScreen((mobile) => {
      const { isMobile } = this.state;
      if (isMobile !== mobile) {
        this.setState({
          isMobile: mobile
        });
      }
    });

    process.browser && this.handleStateChange();
  }

  componentWillUnmount() {
    unenquireScreen(this.enquireHandler);
  }

  handleStateChange() {
    Router.events.on('routeChangeStart', async () => this.setState({ routerChange: true }));
    Router.events.on('routeChangeComplete', async () => this.setState({ routerChange: false }));
  }

  onThemeChange = (theme: string) => {
    const { updateUIValue: updateUI } = this.props;
    updateUI({ theme });
  };

  onCollapseChange = (collapsed) => {
    const { updateUIValue: updateUI } = this.props;
    updateUI({ collapsed });
  };

  render() {
    const {
      children, collapsed, fixedHeader, logo, siteName, theme
    } = this.props;
    const { isMobile, routerChange } = this.state;
    const headerProps = {
      collapsed,
      theme,
      onCollapseChange: this.onCollapseChange
    };

    const sliderMenus = [
      {
        id: 'dashboard',
        name: 'Dashboard',
        icon: <PieChartOutlined />,
        children: [
          {
            id: 'stats',
            name: 'Statistic',
            route: '/dashboard'
          }
        ]
      },
      {
        id: 'blockCountry',
        name: 'Blacklist Country',
        icon: <PieChartOutlined />,
        children: [
          {
            id: 'blockCountries',
            name: 'Blacklist Country',
            route: '/block-country'
          }
        ]
      },
      {
        id: 'posts',
        name: 'Static Pages',
        icon: <ContainerOutlined />,
        children: [
          {
            id: 'post-page',
            name: 'Pages Listing',
            route: '/posts?type=page'
          },
          {
            id: 'page-create',
            name: 'Create page',
            route: '/posts/create?type=page'
          }
        ]
      },
      {
        id: 'menu',
        name: 'FE Menu',
        icon: <MenuOutlined />,
        children: [
          {
            id: 'menu-listing',
            name: 'Menus',
            route: '/menu'
          },
          {
            name: 'Create',
            id: 'create-menu',
            route: '/menu/create'
          }
        ]
      },
      {
        id: 'coupon',
        name: 'Coupons',
        icon: <DollarOutlined />,
        children: [
          {
            id: 'coupon-listing',
            name: 'Coupons',
            route: '/coupon'
          },
          {
            name: 'Create',
            id: 'create-coupon',
            route: '/coupon/create'
          }
        ]
      },
      {
        id: 'banner',
        name: 'Banners',
        icon: <FileImageOutlined />,
        children: [
          {
            id: 'banner-listing',
            name: 'Banners',
            route: '/banner'
          },
          {
            name: 'Upload',
            id: 'upload-banner',
            route: '/banner/upload'
          }
        ]
      },
      {
        id: 'email-template',
        name: 'Email templates',
        icon: <MailOutlined />,
        children: [
          {
            id: 'email-templates-listing',
            name: 'List',
            route: '/email-templates'
          }
        ]
      },
      {
        id: 'accounts',
        name: 'Users',
        icon: <UserOutlined />,
        children: [
          {
            name: 'Users',
            id: 'users',
            route: '/users'
          },
          {
            name: 'Create',
            id: 'users-create',
            route: '/users/create'
          }
        ]
      },
      {
        id: 'performers',
        name: 'Performers',
        icon: <WomanOutlined />,
        children: [
          // {
          //   name: 'Categories',
          //   id: 'performer-categories',
          //   route: '/performer/category'
          // },
          {
            name: 'Performers',
            id: 'performers',
            route: '/performer'
          },
          {
            name: 'Create',
            id: 'create-performers',
            route: '/performer/create'
          }
        ]
      },
      {
        id: 'performers-photos',
        name: 'Photos',
        icon: <CameraOutlined />,
        children: [
          {
            id: 'photo-listing',
            name: 'Photos',
            route: '/photos'
          },
          {
            name: 'Upload',
            id: 'upload-photo',
            route: '/photos/upload'
          },
          {
            name: 'Bulk Upload',
            id: 'bulk-upload-photo',
            route: '/photos/bulk-upload'
          },
          {
            id: 'gallery-listing',
            name: 'Albums',
            route: '/gallery'
          },
          {
            name: 'Create album',
            id: 'create-galleries',
            route: '/gallery/create'
          }
        ]
      },
      {
        id: 'performers-products',
        name: 'Products',
        icon: <SkinOutlined />,
        children: [
          {
            id: 'product-listing',
            name: 'Products',
            route: '/product'
          },
          {
            name: 'Create',
            id: 'create-product',
            route: '/product/create'
          }
        ]
      },
      {
        id: 'videos',
        name: 'Videos',
        icon: <VideoCameraOutlined />,
        children: [
          {
            id: 'video-listing',
            name: 'Videos',
            route: '/video'
          },
          {
            id: 'video-upload',
            name: 'Upload',
            route: '/video/upload'
          },
          {
            id: 'video-bulk-upload',
            name: 'Bulk Upload',
            route: '/video/bulk-upload'
          }
        ]
      },
      {
        id: 'payments',
        name: 'Payment History',
        icon: <DollarOutlined />,
        children: [
          {
            id: 'payment-listing',
            name: 'Payment History',
            route: '/payment'
          }
        ]
      },
      {
        id: 'earning',
        name: 'Earnings',
        icon: <DollarOutlined />,
        route: '/earning'
      },
      {
        id: 'order',
        name: 'Orders',
        icon: <ContainerOutlined />,
        children: [
          {
            id: 'order-listing',
            name: 'Orders Managment',
            route: '/order'
          }
        ]
      },
      {
        id: 'subscription',
        name: 'Subscriptions',
        icon: <HeartOutlined />,
        children: [
          {
            name: 'Subscription Managment',
            id: 'subscription',
            route: '/subscription'
          },
          {
            name: 'Create Subscription',
            id: 'create-subscription',
            route: '/subscription/create'
          }
        ]
      },
      {
        id: 'settings',
        name: 'Settings',
        icon: <PieChartOutlined />,
        children: [
          {
            id: 'system-settings',
            route: '/settings',
            as: '/settings',
            name: 'System settings'
          },
          {
            name: 'Account settings',
            id: 'account-settings',
            route: '/account/settings'
          }
        ]
      }
    ];
    const siderProps = {
      collapsed,
      isMobile,
      logo,
      siteName,
      theme,
      menus: sliderMenus,
      onCollapseChange: this.onCollapseChange,
      onThemeChange: this.onThemeChange
    };

    return (
      <>
        <Layout>
          {isMobile ? (
            <Drawer
              maskClosable
              closable={false}
              onClose={this.onCollapseChange.bind(this, !collapsed)}
              visible={!collapsed}
              placement="left"
              width={200}
              style={{
                padding: 0,
                height: '100vh'
              }}
            >
              <Sider {...siderProps} />
            </Drawer>
          ) : (
            <Sider {...siderProps} />
          )}
          <div className="container" style={{ paddingTop: fixedHeader ? 72 : 0 }} id="primaryLayout">
            <Header {...headerProps} />
            <Layout.Content className="content" style={{ position: 'relative' }}>
              {routerChange && <Loader spinning />}
              {/* <Bread routeList={newRouteList} /> */}
              {children}
            </Layout.Content>
            <BackTop className="backTop" target={() => document.querySelector('#primaryLayout') as any} />
          </div>
        </Layout>
      </>
    );
  }
}

const mapStateToProps = (state: any) => ({
  ...state.ui,
  auth: state.auth
});
const mapDispatchToProps = { updateUIValue, loadUIValue };

export default connect(mapStateToProps, mapDispatchToProps)(PrimaryLayout);
