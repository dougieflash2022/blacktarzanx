export default {
  apiEndpoint: 'http://localhost:9000',
  siteUrl: 'https://example.com',
  maximumSizeUploadImage: 5,
  maximumSizeUploadFile: 5,
  maximumSizeUploadVideo: 2000
};
