/* eslint-disable no-template-curly-in-string */
import { PureComponent, Fragment } from 'react';
import {
  Form, Input, Button, Select, Switch
} from 'antd';
import { IUser, ICountry } from 'src/interfaces';
import { AvatarUpload } from '@components/user/avatar-upload';
import env from 'src/env';

const layout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 16 }
};

const validateMessages = {
  required: 'This field is required!',
  types: {
    email: 'Not a validate email!',
    number: 'Not a validate number!'
  },
  number: {
    range: 'Must be between ${min} and ${max}'
  }
};

interface IProps {
  onFinish: Function;
  user?: IUser;
  updating?: boolean;
  options?: {
    uploadHeaders?: any;
    avatarUploadUrl?: string;
    onAvatarUploaded?: Function;
    beforeUpload?: Function;
  };
  countries: ICountry[];
}

export class AccountForm extends PureComponent<IProps> {
  render() {
    const {
      onFinish, user, updating, options
    } = this.props;
    const {
      uploadHeaders, avatarUploadUrl, onAvatarUploaded, beforeUpload
    } = options;
    return (
      <Form
        {...layout}
        name="nest-messages"
        onFinish={onFinish.bind(this)}
        validateMessages={validateMessages}
        initialValues={
          user || {
            status: 'active',
            gender: 'male',
            roles: ['user']
          }
        }
      >
        <Form.Item name="firstName" label="First name" rules={[{ required: true }]}>
          <Input />
        </Form.Item>
        <Form.Item name="lastName" label="Last name" rules={[{ required: true }]}>
          <Input />
        </Form.Item>
        <Form.Item name="gender" label="Gender" rules={[{ required: true }]}>
          <Select>
            <Select.Option key="male" value="male">
              Male
            </Select.Option>
            <Select.Option key="female" value="female">
              Female
            </Select.Option>
            <Select.Option key="transgender" value="transgender">
              Transgender
            </Select.Option>
          </Select>
        </Form.Item>
        <Form.Item
          name="username"
          label="Username"
          rules={[{ required: true }, {
            pattern: new RegExp(/^[a-z0-9]+$/g),
            message: 'Username must contain only Alphabets & Numbers'
          }, { min: 3 }]}
        >
          <Input placeholder="Unique, lowercase and number, no space or special chars" />
        </Form.Item>
        <Form.Item name="email" label="Email" rules={[{ type: 'email', required: true }]}>
          <Input />
        </Form.Item>
        {!user && (
          <>
            <Form.Item name="password" label="Password" rules={[{ required: true }, { min: 6 }]}>
              <Input.Password placeholder="User password" />
            </Form.Item>
            <Form.Item name="rePassword" label="Confirm password" rules={[{ required: true }, { min: 6 }]}>
              <Input.Password placeholder="Confirm user password" />
            </Form.Item>
          </>
        )}
        {/* <Form.Item name="country" label="Country" rules={[{ required: true }]}>
          <Select
            showSearch
            filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
            {countries.map(country => (
              <Select.Option key={country.code} value={country.code}>
                {country.name}
              </Select.Option>
            ))}
          </Select>
        </Form.Item> */}
        <>
          {/* <Form.Item name="balance" label="Balance">
              <InputNumber />
            </Form.Item> */}
          <Form.Item name="roles" label="Roles" rules={[{ required: true }]}>
            <Select mode="multiple">
              <Select.Option key="user" value="user">
                User
              </Select.Option>
              <Select.Option key="admin" value="admin">
                Admin
              </Select.Option>
            </Select>
          </Form.Item>
          <Form.Item name="status" label="Status" rules={[{ required: true }]}>
            <Select>
              <Select.Option key="active" value="active">
                Active
              </Select.Option>
              <Select.Option key="inactive" value="inactive">
                Inactive
              </Select.Option>
              <Select.Option key="pending-email-confirmation" value="pending-email-confirmation">
                Pending email confirmation
              </Select.Option>
            </Select>
          </Form.Item>
          <Form.Item name="verifiedEmail" label="Verified Email" valuePropName="checked">
            <Switch defaultChecked={user && user.verifiedEmail ? user.verifiedEmail : false} />
          </Form.Item>
        </>
        <Form.Item
          label="Avatar"
          help={`Image must be smaller than ${env.maximumSizeUploadImage || 5}MB!`}
        >
          {/* <Avatar alt="Avatar" /> */}
          <AvatarUpload
            headers={uploadHeaders}
            uploadUrl={avatarUploadUrl}
            onUploaded={onAvatarUploaded}
            imageUrl={user ? user.avatar : ''}
            uploadNow={!!user}
            beforeUpload={beforeUpload}
          />
        </Form.Item>
        <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 4 }}>
          <Button type="primary" htmlType="submit" loading={updating}>
            Submit
          </Button>
        </Form.Item>
      </Form>
    );
  }
}
