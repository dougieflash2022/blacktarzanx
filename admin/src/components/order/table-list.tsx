import { Table, Tag } from 'antd';
import {
  EyeOutlined
} from '@ant-design/icons';
import { IOrder } from 'src/interfaces';
import { formatDate } from '@lib/date';
import Link from 'next/link';

interface IProps {
  dataSource: IOrder[];
  pagination: {};
  rowKey: string;
  loading: boolean;
  onChange: Function;
}

const OrderTableList = ({
  dataSource,
  pagination,
  rowKey,
  loading,
  onChange
}: IProps) => {
  const columns = [
    {
      title: 'Order number',
      dataIndex: 'orderNumber',
      key: 'orderNumber'
    },
    {
      title: 'Buyer',
      dataIndex: 'buyerId',
      key: 'buyerId',
      sorter: true,
      render(data, record) {
        return (
          <span>
            {`${record.buyer?.firstName} ${record.buyer?.lastName}`}
            {' '}
            - @
            {`${record.buyer?.username}`}
          </span>
        );
      }
    },
    {
      title: 'Seller',
      dataIndex: 'sellerId',
      key: 'sellerId',
      sorter: true,
      render(data, record) {
        return (
          <span>
            {`${record.seller?.firstName} ${record.seller?.lastName}`}
            {' '}
            - @
            {`${record.seller?.username}`}
          </span>
        );
      }
    },
    {
      title: 'Product type',
      dataIndex: 'productType',
      key: 'productType'
    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name'
    },
    {
      title: 'Quantity',
      dataIndex: 'quantity',
      sorter: true,
      render(quantity) {
        return (
          <span>{quantity}</span>
        );
      }
    },
    {
      title: 'Total Price',
      dataIndex: 'totalPrice',
      sorter: true,
      render(data, record) {
        return (
          <span>
            $
            {record.totalPrice && record.totalPrice.toFixed(2)}
          </span>
        );
      }
    },
    {
      title: 'Delivery Status',
      dataIndex: 'deliveryStatus',
      render(status: string) {
        switch (status) {
          case 'processing':
            return <Tag color="default">Processing</Tag>;
          case 'shipping':
            return <Tag color="warning">Shipping</Tag>;
          case 'delivered':
            return <Tag color="success">Delivered</Tag>;
          case 'refunded':
            return <Tag color="danger">Refunded</Tag>;
          default: return <Tag>{status}</Tag>;
        }
      }
    },
    {
      title: 'Last updated at',
      dataIndex: 'createdAt',
      sorter: true,
      render(date: Date) {
        return <span>{formatDate(date)}</span>;
      }
    },
    {
      title: '#',
      dataIndex: '_id',
      sorter: true,
      render(id: string) {
        return <Link href={{ pathname: '/order/detail', query: { id } }}><a><EyeOutlined /></a></Link>;
      }
    }
  ];
  return (
    <Table
      dataSource={dataSource}
      columns={columns}
      pagination={pagination}
      rowKey={rowKey}
      loading={loading}
      onChange={onChange.bind(this)}
    />
  );
};
export default OrderTableList;
