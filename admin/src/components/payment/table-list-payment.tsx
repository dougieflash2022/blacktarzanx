import { PureComponent } from 'react';
import {
  Table, Tag
} from 'antd';
import { formatDate } from '@lib/date';

interface IProps {
  dataSource: [];
  rowKey: string;
  loading: boolean;
  pagination: {};
  onChange: Function;
}

export class TableListPaymentTransaction extends PureComponent<IProps> {
  render() {
    const columns = [
      {
        title: 'Seller',
        dataIndex: 'seller',
        key: 'seller',
        sorter: true,
        render(data, record) {
          return (
            <div>
              {record?.sellerSource === 'system' ? 'Admin' : record?.seller?.username}
            </div>
          );
        }
      },
      {
        title: 'Buyer',
        dataIndex: 'buyer',
        key: 'buyer',
        sorter: true,
        render(buyer) {
          return (
            <div>
              {buyer?.username}
              <br />
              {buyer?.email}
            </div>
          );
        }
      },
      {
        title: 'Type',
        dataIndex: 'type',
        key: 'type',
        sorter: true,
        render(type: number) {
          return <Tag color="orange">{type}</Tag>;
        }
      },
      {
        title: 'Description',
        render(data, record) {
          return record.details?.map((p) => (
            <p key={p._id}>
              <span>{p.name}</span>
            </p>
          ));
        }
      },
      {
        title: 'Original price',
        dataIndex: 'originalPrice',
        sorter: true,
        render(data, record) {
          const originalPrice = record.couponInfo
            ? record.totalPrice / (1 - record.couponInfo.value)
            : record.totalPrice;
          return (
            <span>
              $
              {originalPrice.toFixed(2)}
            </span>
          );
        }
      },
      {
        title: 'End Price',
        dataIndex: 'totalPrice',
        sorter: true,
        render(data, record) {
          return (
            <span>
              $
              {record.totalPrice?.toFixed(2)}
            </span>
          );
        }
      },
      {
        title: 'Discount',
        dataIndex: 'couponInfo',
        sorter: true,
        render(data, record) {
          return record.couponInfo ? (
            <span>
              {`${record.couponInfo.value * 100}%`}
            </span>
          ) : '';
        }
      },
      {
        title: 'Payment status',
        dataIndex: 'status',
        sorter: true,
        render(status: string) {
          switch (status) {
            case 'pending':
              return <Tag color="orange">Pending</Tag>;
            case 'paid':
              return <Tag color="green">Success</Tag>;
            case 'cancel':
              return <Tag color="red">Cancel</Tag>;
            default: return <Tag color="red">Pending</Tag>;
          }
        }
      },
      {
        title: 'Last update',
        dataIndex: 'updatedAt',
        sorter: true,
        fixed: 'right' as 'right',
        render(date: Date) {
          return <span>{formatDate(date)}</span>;
        }
      }
    ];
    const {
      dataSource, rowKey, loading, pagination, onChange
    } = this.props;
    return (
      <Table
        dataSource={dataSource}
        columns={columns}
        rowKey={rowKey}
        loading={loading}
        pagination={pagination}
        onChange={onChange.bind(this)}
        scroll={{ x: '150vw', y: '90vh' }}
      />
    );
  }
}
