import { PureComponent } from 'react';
import {
  Form, Input, Button, message
} from 'antd';
import { ICCbillSetting, CCBillPaymentGateway } from 'src/interfaces';

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 16 }
};

const validateMessages = {
  required: 'This field is required!'
};

interface IProps {
  onFinish: Function;
  ccbillSetting?: ICCbillSetting;
  submiting?: boolean;
}

export class CCbillSettingForm extends PureComponent<IProps> {
  render() {
    const { ccbillSetting, onFinish, submiting } = this.props;
    return (
      <Form
        // {...layout}
        layout="vertical"
        name="form-performer"
        onFinish={onFinish.bind(this)}
        onFinishFailed={() => message.error('Please complete the required fields.')}
        validateMessages={validateMessages}
        initialValues={
          ccbillSetting
            ? {
              ...ccbillSetting.value
            }
            : ({
              subAccountNumber: '',
              flexformId: '',
              salt: ''
            } as CCBillPaymentGateway)
        }
      >

        <Form.Item
          name="subscriptionSubAccountNumber"
          label="Subscription sub-account number"
          rules={[{ required: true, message: 'Please enter sub account number' }]}
        >
          <Input placeholder="Enter subscription sub-account number" />
        </Form.Item>
        <Form.Item
          name="singlePurchaseSubAccountNumber"
          label="Single purchase sub-account number"
          rules={[{ required: true, message: 'Please enter sub account number' }]}
        >
          <Input placeholder="Enter single purchase sub-account number" />
        </Form.Item>
        <Form.Item
          name="flexformId"
          label="Flex form Id"
          rules={[{ required: true, message: 'Please enter flex form Id' }]}
        >
          <Input placeholder="Enter flex form Id" />
        </Form.Item>
        <Form.Item name="salt" label="Salt key" help="If salt is not provided, will get from global settings.">
          <Input placeholder="Enter salt key" />
        </Form.Item>
        <Form.Item wrapperCol={{ ...layout.wrapperCol }}>
          <Button type="primary" htmlType="submit" loading={submiting}>
            Submit
          </Button>
        </Form.Item>
      </Form>
    );
  }
}
