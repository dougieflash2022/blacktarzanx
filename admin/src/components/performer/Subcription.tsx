import { PureComponent, createRef } from 'react';
import {
  Form, Button, message, InputNumber
} from 'antd';
import { IPerformerUpdate, IPerformerCreate } from 'src/interfaces';

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 16 }
};

const validateMessages = {
  required: 'This field is required!'
};

interface IProps {
  onFinish: Function;
  performer?: IPerformerUpdate;
  submiting?: boolean;
  ref?: Function;
}

export class SubscriptionForm extends PureComponent<IProps> {
  private formRef = createRef() as any;

  formRefSubmit() {
    this.formRef.current.submit();
  }

  render() {
    const { performer, onFinish, submiting } = this.props;

    return (
      <Form
        ref={this.formRef}
        {...layout}
        name="form-performer"
        onFinish={onFinish.bind(this)}
        onFinishFailed={() => message.error('Please complete the required fields in tab general info')}
        validateMessages={validateMessages}
        initialValues={
          performer || (({
            yearlyPrice: 1,
            monthlyPrice: 1
          } as unknown) as IPerformerCreate)
        }
      >
        <Form.Item name="yearlyPrice" label="Yearly Subscription Price" rules={[{ required: true }]}>
          <InputNumber min={1} max={9999} />
        </Form.Item>
        <Form.Item name="monthlyPrice" label="Monthly Subscription Price" rules={[{ required: true }]}>
          <InputNumber min={1} max={9999} />
        </Form.Item>
        <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 4 }}>
          <Button type="primary" htmlType="submit" loading={submiting}>
            Submit
          </Button>
        </Form.Item>
      </Form>
    );
  }
}
