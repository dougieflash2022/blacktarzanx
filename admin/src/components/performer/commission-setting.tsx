import { PureComponent } from 'react';
import {
  Form, Button, message, InputNumber
} from 'antd';
import { ICommissionSetting } from 'src/interfaces';

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 16 }
};

const validateMessages = {
  required: 'This field is required!'
};

interface IProps {
  onFinish: Function;
  commissionSetting?: ICommissionSetting;
  submiting?: boolean;
}

export class CommissionSettingForm extends PureComponent<IProps> {
  render() {
    const { commissionSetting, onFinish, submiting } = this.props;
    return (
      <Form
        layout="vertical"
        name="form-performer"
        onFinish={onFinish.bind(this)}
        onFinishFailed={() => message.error('Please complete the required fields.')}
        validateMessages={validateMessages}
        initialValues={
          commissionSetting || ({
            monthlySubscriptionCommission: 0.1,
            yearlySubscriptionCommission: 0.1,
            videoSaleCommission: 0.1,
            productSaleCommission: 0.1
          } as ICommissionSetting)
        }
      >
        <Form.Item name="monthlySubscriptionCommission" label="Monthly Subscription">
          <InputNumber min={0.01} max={0.99} style={{ width: '100%' }} />
        </Form.Item>
        <Form.Item name="yearlySubscriptionCommission" label="Yearly Subscription">
          <InputNumber min={0.01} max={0.99} style={{ width: '100%' }} />
        </Form.Item>
        <Form.Item name="videoSaleCommission" label="Video sale commission">
          <InputNumber min={0.01} max={0.99} style={{ width: '100%' }} />
        </Form.Item>
        <Form.Item name="productSaleCommission" label="Product sale commission">
          <InputNumber min={0.01} max={0.99} style={{ width: '100%' }} />
        </Form.Item>
        <Form.Item wrapperCol={{ ...layout.wrapperCol }}>
          <Button type="primary" htmlType="submit" loading={submiting}>
            Submit
          </Button>
        </Form.Item>
      </Form>
    );
  }
}
