import { PureComponent } from 'react';
import {
  Button
} from 'antd';
import { } from '@ant-design/icons';
import { IPerformer } from 'src/interfaces';

interface IProps {
  type: string;
  performer: IPerformer;
  onFinish: Function;
  submiting: boolean;
}

export class ConfirmSubscriptionPerformerForm extends PureComponent<IProps> {
  render() {
    const {
      onFinish, submiting = false, performer, type
    } = this.props;
    return (
      <div className="text-center">
        <div className="tip-performer">
          <img alt="" src={performer?.avatar || '/no-avatar.png'} style={{ width: '100px', borderRadius: '50%' }} />
          <div>
            {performer?.name}
            &nbsp;
            <small>
              @
              {performer?.username}
            </small>
          </div>
        </div>
        <div style={{ margin: '20px 0' }}>
          <p style={{ textTransform: 'capitalize' }}>
            {type}
            {' '}
            subscription by
            {' '}
            $
            {' '}
            {type === 'monthly' ? performer.monthlyPrice.toFixed(2) : performer.yearlyPrice.toFixed(2)}
          </p>
        </div>
        <Button type="primary" disabled={submiting} loading={submiting} onClick={() => onFinish()}>Confirm Subscribe</Button>
      </div>
    );
  }
}
