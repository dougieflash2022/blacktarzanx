import { Descriptions, Tag } from 'antd';
import { PureComponent } from 'react';
import { ICountry, IPerformer } from 'src/interfaces';
import './performer.less';

interface IProps {
  performer: IPerformer;
  countries: ICountry[];
}

export class PerformerInfo extends PureComponent<IProps> {
  render() {
    const { performer, countries = [] } = this.props;
    const country = countries.length && countries.find((c) => c.code === performer.country);
    return (
      <>
        <p>{performer?.bio}</p>
        <Descriptions className="performer-country">
          {performer?.country && (
            <Descriptions.Item key="country">
              <img alt="performer-country" src={country?.flag} height="20px" />
              &nbsp;
              {country?.name}
            </Descriptions.Item>
          )}
          {performer.state && <Descriptions.Item label="State (County)">{performer.state}</Descriptions.Item>}
          {performer.city && <Descriptions.Item label="City">{performer.city}</Descriptions.Item>}
        </Descriptions>
        <Descriptions className="performer-info">
          {performer.gender && (
          <Descriptions.Item label={null}>
            <Tag color="magenta">
              <img
                height="15px"
                alt="performer-gender"
                // eslint-disable-next-line no-nested-ternary
                src={performer.gender === 'male' ? '/male.png' : performer.gender === 'female' ? '/female.png' : '/transgender.png'}
              />
              {' '}
              {performer.gender}
            </Tag>
          </Descriptions.Item>
          )}
          {performer.height && <Descriptions.Item label="Height">{performer.height}</Descriptions.Item>}
          {performer.weight && <Descriptions.Item label="Weight">{performer.weight}</Descriptions.Item>}
          {performer.eyes && <Descriptions.Item label="Eyes color">{performer.eyes}</Descriptions.Item>}
          {performer.sexualPreference && <Descriptions.Item label="Sexual reference">{performer.sexualPreference}</Descriptions.Item>}
        </Descriptions>
      </>
    );
  }
}
