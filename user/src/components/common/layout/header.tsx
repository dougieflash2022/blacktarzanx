import { PureComponent } from 'react';
import {
  Layout, Avatar, Badge, Tooltip, Drawer, Divider
} from 'antd';
import { connect } from 'react-redux';
import Link from 'next/link';
import { IUser } from 'src/interfaces';
import { logout } from '@redux/auth/actions';
import {
  ShoppingCartOutlined, MessageOutlined, HomeOutlined, LikeOutlined,
  ContactsOutlined, StarOutlined, SearchOutlined, HeartOutlined,
  UserOutlined, ShoppingOutlined, LinkOutlined, LogoutOutlined,
  VideoCameraOutlined, PictureOutlined
} from '@ant-design/icons';
import './header.less';
import { withRouter, Router as RouterEvent } from 'next/router';
import { addCart } from 'src/redux/cart/actions';
import { cartService, messageService, authService } from 'src/services';
import { Event, SocketContext } from 'src/socket';
import SearchBar from './search-bar';

interface IProps {
  currentUser?: IUser;
  logout?: Function;
  router: any;
  ui?: any;
  cart?: any;
  addCart: Function;
}

class Header extends PureComponent<IProps> {
  state = {
    totalNotReadMessage: 0,
    openSearch: false,
    openProfile: false
  };

  async componentDidMount() {
    if (process.browser) {
      const { cart, currentUser, addCart: addCartHandler } = this.props;
      RouterEvent.events.on(
        'routeChangeStart',
        async () => this.setState({ openProfile: false })
      );
      if (!cart || (cart && cart.items.length <= 0)) {
        if (currentUser._id) {
          const existCart = await cartService.getCartByUser(currentUser._id);
          if (existCart && existCart.length > 0) {
            addCartHandler(existCart);
          }
        }
      }
    }
  }

  async componentDidUpdate(prevProps: any) {
    const { cart, currentUser, addCart: addCartHandler } = this.props;
    if (prevProps.currentUser !== currentUser) {
      if (!cart || (cart && cart.items.length <= 0)) {
        if (currentUser._id && process.browser) {
          const existCart = await cartService.getCartByUser(currentUser._id);
          if (existCart && existCart.length > 0) {
            addCartHandler(existCart);
          }
        }
      }
      if (currentUser && currentUser._id) {
        const data = await (await messageService.countTotalNotRead()).data;
        if (data) {
          // eslint-disable-next-line react/no-did-update-set-state
          this.setState({ totalNotReadMessage: data.total });
        }
      }
    }
  }

  async handleMessage(event) {
    event && this.setState({ totalNotReadMessage: event.total });
  }

  async beforeLogout() {
    const { logout: logoutHandler } = this.props;
    const token = authService.getToken();
    const socket = this.context;
    token
      && socket
      && (await socket.emit('auth/logout', {
        token
      }));
    socket && socket.close();
    logoutHandler();
  }

  render() {
    const {
      currentUser, router, ui, cart
    } = this.props;
    const { totalNotReadMessage, openSearch, openProfile } = this.state;

    return (
      <div className="main-header">
        <Event
          event="nofify_read_messages_in_conversation"
          handler={this.handleMessage.bind(this)}
        />
        <div className="main-container">
          <Layout.Header className="header" id="layoutHeader">
            <div className="nav-bar">
              <div className="left-conner hide-mobile">
                <Link href="/home">
                  <a className="logo-nav">
                    {ui?.logo ? (
                      <img
                        alt="logo"
                        src={ui?.logo}
                        height="64"
                      />
                    ) : <span>{ui?.siteName}</span>}
                  </a>
                </Link>
                <div className="hide-tablet"><SearchBar /></div>
              </div>
              <div className="mid-conner">
                <ul className="nav-icons">
                  {currentUser._id && currentUser.isPerformer && (
                    <li
                      className={
                        router.asPath === `/model/${currentUser.username}`
                          ? 'active'
                          : ''
                      }
                    >
                      <Link
                        href={{
                          pathname: '/model/profile',
                          query: { username: currentUser.username }
                        }}
                        as={`/model/${currentUser.username}`}
                      >
                        <Tooltip title="My Profile">
                          <a><HomeOutlined /></a>
                        </Tooltip>
                      </Link>
                    </li>
                  )}
                  {currentUser._id && !currentUser.isPerformer && (
                    <li
                      className={router.pathname === '/home' ? 'active' : ''}
                    >
                      <Link href="/home">
                        <Tooltip title="Home">
                          <a><HomeOutlined /></a>
                        </Tooltip>
                      </Link>
                    </li>
                  )}
                  {currentUser._id && (
                    <>
                      <li className={router.pathname === '/model' ? 'active' : ''}>
                        <Link href={{ pathname: '/model' }} as="/model">
                          <Tooltip title="Models">
                            <a><StarOutlined /></a>
                          </Tooltip>
                        </Link>
                      </li>
                      <li
                        className={router.pathname === '/contact' ? 'active' : ''}
                      >
                        <Link href="/contact">
                          <Tooltip title="Contact">
                            <a><ContactsOutlined /></a>
                          </Tooltip>
                        </Link>
                      </li>
                      <li
                        className={
                          router.pathname === '/messages' ? 'active' : ''
                        }
                      >
                        <Link href="/messages">
                          <Tooltip title="Messenger">
                            <a>
                              <MessageOutlined />
                              <Badge
                                className="cart-total"
                                count={totalNotReadMessage}
                                showZero
                              />
                            </a>
                          </Tooltip>
                        </Link>
                      </li>
                    </>
                  )}
                  {currentUser._id && !currentUser.isPerformer && (
                    <li className={router.pathname === '/cart' ? 'active' : ''}>
                      <Link href="/cart">
                        <Tooltip title="Shopping Cart">
                          <a>
                            <ShoppingCartOutlined />
                            <Badge
                              className="cart-total"
                              count={cart.total}
                              showZero
                            />
                          </a>
                        </Tooltip>
                      </Link>
                    </li>
                  )}
                  {currentUser._id && (
                    <li className="hide-desktop" aria-hidden onClick={() => this.setState({ openSearch: !openSearch })}>
                      <a className="search-mobile"><SearchOutlined /></a>
                    </li>
                  )}
                  {!currentUser._id && [
                    <li key="login" className={router.pathname === '/auth/login' ? 'active' : ''}>
                      <Link href="/auth/login">
                        <a>Login</a>
                      </Link>
                    </li>,
                    <li key="signup" className={router.pathname === '/' ? 'active' : ''}>
                      <Link href="/">
                        <a>Sign Up</a>
                      </Link>
                    </li>
                  ]}
                  {currentUser._id && (
                    <li aria-hidden onClick={() => this.setState({ openProfile: true })}>
                      <Avatar src={currentUser.avatar || '/no-avatar.png'} />
                    </li>
                  )}
                </ul>
              </div>
              <Drawer
                title={null}
                closable
                onClose={() => this.setState({ openSearch: false })}
                visible={openSearch}
                key="search-drawer"
                width={350}
              >
                <SearchBar />
              </Drawer>
              <Drawer
                title={(
                  <div className="profile-user">
                    <img src={currentUser.avatar || '/no-avatar.png'} alt="logo" />
                    <a className="profile-name">
                      {currentUser.name}
                      <span>
                        @
                        {currentUser.username}
                      </span>
                    </a>
                  </div>
                )}
                closable
                onClose={() => this.setState({ openProfile: false })}
                visible={openProfile}
                key="profile-drawer"
                className="profile-drawer"
                width={280}
              >
                {currentUser.isPerformer && (
                  <div className="profile-menu-item">
                    <Link href="/model/account" as="/model/account">
                      <div className={router.pathname === '/model/account' ? 'menu-item active' : 'menu-item'}>
                        <UserOutlined />
                        {' '}
                        Edit Profile
                      </div>
                    </Link>
                    <Link href={{ pathname: '/model/my-subscriber' }} as="/model/my-subscriber">
                      <div className={router.pathname === '/model/my-subscriber' ? 'menu-item active' : 'menu-item'}>
                        <HeartOutlined />
                        {' '}
                        Subscribers
                      </div>
                    </Link>
                    <Divider />
                    <Link href="/model/my-video" as="/model/my-video">
                      <div className={router.pathname === '/model/my-video' ? 'menu-item active' : 'menu-item'}>
                        <VideoCameraOutlined />
                        {' '}
                        Videos
                      </div>
                    </Link>
                    <Link href="/model/my-gallery/listing" as="/model/my-gallery/listing">
                      <div className={router.pathname === '/model/my-gallery/listing' ? 'menu-item active' : 'menu-item'}>
                        <PictureOutlined />
                        {' '}
                        Galleries
                      </div>
                    </Link>
                    <Link href="/model/my-store" as="/model/my-store">
                      <div className={router.pathname === '/model/my-store' ? 'menu-item active' : 'menu-item'}>
                        <ShoppingOutlined />
                        {' '}
                        Store
                      </div>
                    </Link>
                    <Divider />
                    <Link href={{ pathname: '/model/my-order' }} as="/model/my-order">
                      <div className={router.pathname === '/model/my-order' ? 'menu-item active' : 'menu-item'}>
                        <ShoppingCartOutlined />
                        {' '}
                        Orders
                      </div>
                    </Link>
                    <Link href="/model/earning" as="/model/earning">
                      <div className={router.pathname === '/model/earning' ? 'menu-item active' : 'menu-item'}>
                        <LinkOutlined />
                        {' '}
                        Earnings
                      </div>
                    </Link>
                    <Link href={{ pathname: '/model/top-models' }} as="/model/top-models">
                      <div className={router.pathname === '/model/top-models' ? 'menu-item active' : 'menu-item'}>
                        <StarOutlined />
                        {' '}
                        Top models
                      </div>
                    </Link>
                    <Divider />
                    <div aria-hidden className="menu-item" onClick={() => this.beforeLogout()}>
                      <LogoutOutlined />
                      {' '}
                      Sign Out
                    </div>
                  </div>
                )}
                {!currentUser.isPerformer && (
                  <div className="profile-menu-item">
                    <Link href="/user/account" as="/user/account">
                      <div className={router.pathname === '/user/account' ? 'menu-item active' : 'menu-item'}>
                        <UserOutlined />
                        {' '}
                        Edit Profile
                      </div>
                    </Link>
                    <Divider />
                    <Link href="/user/my-favourite" as="/user/my-favourite">
                      <div className={router.pathname === '/user/my-favourite' ? 'menu-item active' : 'menu-item'}>
                        <LikeOutlined />
                        {' '}
                        Favourite Vids
                      </div>
                    </Link>
                    <Link href="/user/my-wishlist" as="/user/my-wishlist">
                      <div className={router.pathname === '/user/my-wishlist' ? 'menu-item active' : 'menu-item'}>
                        <HeartOutlined />
                        {' '}
                        Wishlist Vids
                      </div>
                    </Link>
                    <Link href="/user/my-subscription" as="/user/my-subscription">
                      <div className={router.pathname === '/user/my-subscription' ? 'menu-item active' : 'menu-item'}>
                        <StarOutlined />
                        {' '}
                        Subscriptions
                      </div>
                    </Link>
                    <Divider />
                    <Link href="/user/orders" as="/user/orders">
                      <div className={router.pathname === '/user/orders' ? 'menu-item active' : 'menu-item'}>
                        <ShoppingCartOutlined />
                        {' '}
                        Orders
                      </div>
                    </Link>
                    <Link href="/user/payment-history" as="/user/payment-history">
                      <div className={router.pathname === '/user/payment-history' ? 'menu-item active' : 'menu-item'}>
                        <LinkOutlined />
                        {' '}
                        Transactions
                      </div>
                    </Link>
                    <Divider />
                    <div className="menu-item" aria-hidden onClick={() => this.beforeLogout()}>
                      <LogoutOutlined />
                      {' '}
                      Sign Out
                    </div>
                  </div>
                )}
              </Drawer>
            </div>
          </Layout.Header>
        </div>
      </div>
    );
  }
}

Header.contextType = SocketContext;
const mapState = (state: any) => ({
  currentUser: state.user.current,
  ui: state.ui,
  cart: state.cart
});
const mapDispatch = { logout, addCart };
export default withRouter(connect(mapState, mapDispatch)(Header)) as any;
