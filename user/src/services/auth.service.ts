import cookie from 'js-cookie';
import { ILogin, IFanRegister, IForgot } from 'src/interfaces';
import { APIRequest, TOKEN } from './api-request';

export class AuthService extends APIRequest {
  public async login(data: ILogin) {
    if (data.loginUsername) {
      return this.post('/auth/users/login/username', data);
    }
    return this.post('/auth/users/login/email', data);
  }

  public async loginPerformer(data: ILogin) {
    if (data.loginUsername) {
      return this.post('/auth/performers/login/username', data);
    }
    return this.post('/auth/performers/login/email', data);
  }

  setToken(token: string, role: string, remember = false): void {
    // https://github.com/js-cookie/js-cookie
    // since Safari does not support, need a better solution
    const expired = { expires: !remember ? 1 : 365 };
    cookie.set(TOKEN, token, expired);
    cookie.set('role', role, expired);
    this.setAuthHeaderToken(token);
  }

  getToken(): string {
    return cookie.get(TOKEN);
  }

  getUserRole() {
    return cookie.get('role');
  }

  removeToken(): void {
    cookie.remove(TOKEN);
    cookie.remove('role');
  }

  removeRemember(): void {
    process.browser && localStorage.removeItem('rememberMe');
  }

  updatePassword(password: string, type?: string, source?: string) {
    return this.put('/auth/users/me/password', { type, password, source });
  }

  resetPassword(data: IForgot) {
    return this.post('/auth/users/forgot', data);
  }

  public async register(data: IFanRegister) {
    return this.post('/auth/users/register', data);
  }

  public async registerPerformer(documents: {
    file: File;
    fieldname: string;
  }[], data: any, onProgress?: Function) {
    return this.upload('/auth/performers/register', documents, {
      onProgress,
      customData: data
    });
  }
}

export const authService = new AuthService();
