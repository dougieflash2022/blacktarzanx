import { PureComponent } from 'react';
import Head from 'next/head';
import { Layout, message } from 'antd';
import { postService } from '@services/post.service';
import { connect } from 'react-redux';
import Router from 'next/router';

interface IProps {
  ui: any;
  id: string;
}
class PostDetail extends PureComponent<IProps> {
  static authenticate: boolean = true;

  static noredirect: boolean = true;

  state = {
    // fetching: true,
    post: null
  };

  static async getInitialProps({ ctx }: any) {
    const { query } = ctx;
    return query;
  }

  async componentDidMount() {
    this.getPost();
  }

  async componentDidUpdate(prevProps: IProps) {
    const { id } = this.props;
    if (prevProps.id !== id) {
      this.getPost();
    }
  }

  async getPost() {
    try {
      const { id } = this.props;
      const resp = await postService.findById(id as string);
      this.setState({ post: resp.data });
    } catch (e) {
      const err = await e;
      message.error(err?.message || 'Error occured, pleasy try again later');
      Router.back();
    }
  }

  render() {
    const { post } = this.state;
    const { ui } = this.props;
    return (
      <Layout>
        <Head>
          <title>
            {`${ui?.siteName} | ${post?.title}`}
          </title>
        </Head>
        <div className="main-container">
          <div className="page-container">
            <div className="page-heading">{post?.title}</div>
            <div
              className="page-content"
                  // eslint-disable-next-line react/no-danger
              dangerouslySetInnerHTML={{ __html: post?.content }}
            />
          </div>
        </div>
      </Layout>
    );
  }
}
const mapProps = (state: any) => ({
  ui: state.ui
});

export default connect(mapProps)(PostDetail);
