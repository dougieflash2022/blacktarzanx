import { PureComponent } from 'react';
import {
  Row, Col, Layout, Pagination, Spin, Select
} from 'antd';
import { connect } from 'react-redux';
import { getList } from '@redux/performer/actions';
import PerformerCard from '@components/performer/card';
import Head from 'next/head';
import { SearchFilter } from '@components/common/base/search-filter';
import { IUIConfig } from 'src/interfaces/';
import '@components/performer/performer.less';
import { StarOutlined } from '@ant-design/icons';
import Router from 'next/router';

interface IProps {
  getList: Function;
  performerState: any;
  ui: IUIConfig;
}

class Performers extends PureComponent<IProps> {
  static authenticate: boolean = true;

  static noredirect: boolean = true;

  state = {
    offset: 0,
    limit: 12,
    sort: 'popular'
  };

  async componentDidMount() {
    const { getList: getListHandler } = this.props;
    const { limit, offset, sort } = this.state;
    try {
      getListHandler({
        limit,
        offset,
        sort
      });
    } catch (e) {
      Router.back();
    }
  }

  async handleFilter(values: any) {
    const { getList: getListHandler } = this.props;
    const { limit } = this.state;
    await this.setState({ offset: 0 });
    getListHandler({
      limit,
      offset: 0,
      ...values
    });
  }

  async handleSort(value: any) {
    this.setState({ sort: value });
    const { getList: getListHandler } = this.props;
    const { limit } = this.state;
    await this.setState({ offset: 0 });
    getListHandler({
      limit,
      offset: 0,
      sort: value
    });
  }

  pageChanged = async (page: number) => {
    const { getList: getListHandler } = this.props;
    const { limit, sort } = this.state;
    await this.setState({ offset: page });
    getListHandler({
      sort,
      limit,
      offset: (page - 1) * 12
    });
  };

  render() {
    const {
      performerState = {
        requesting: false,
        error: null,
        success: false,
        data: null
      },
      ui
    } = this.props;
    const {
      limit, offset, sort
    } = this.state;
    const performers = performerState?.data?.data || [];
    const total = performerState?.data?.total || 0;
    const { requesting: isLoading } = performerState;

    return (
      <>
        <Head>
          <title>
            {ui && ui.siteName}
            {' '}
            | Models
          </title>
        </Head>
        <Layout>
          <div className="main-container">
            <SearchFilter
              genders={[
                { key: '', text: 'All' },
                { key: 'male', text: 'Male' },
                { key: 'female', text: 'Female' },
                { key: 'transgender', text: 'Transgender' }
              ]}
              onSubmit={this.handleFilter.bind(this)}
            />
            <div className="hr-divider" />
            <div className="main-background">
              <Row>
                <Col xs={{ span: 24 }} className="md-heading">
                  <span>
                    <StarOutlined />
                    {' '}
                    {total}
                    {' '}
                    Models
                  </span>
                  <span className="sort-model">
                    <Select value={sort} onChange={this.handleSort.bind(this)}>
                      <Select.Option value="">
                        Sort
                      </Select.Option>
                      <Select.Option value="latest">
                        Latest
                      </Select.Option>
                      <Select.Option value="oldest">
                        Oldest
                      </Select.Option>
                      <Select.Option value="popular">
                        Most Popular
                      </Select.Option>
                    </Select>
                  </span>
                </Col>

                {performers && performers.length > 0
                    && !isLoading
                    && performers.map((p: any) => (
                      <Col xs={12} sm={12} md={6} lg={6} key={p._id}>
                        <PerformerCard performer={p} />
                      </Col>
                    ))}
                {!total && !isLoading && <p>No model was found.</p>}
                {isLoading && (
                <div className="text-center">
                  <Spin />
                </div>
                )}
                {total && total > limit && !isLoading ? (
                  <div className="paging">
                    <Pagination
                      showQuickJumper
                      defaultCurrent={offset + 1}
                      total={total}
                      pageSize={limit}
                      onChange={this.pageChanged}
                    />
                  </div>
                ) : null}
              </Row>
            </div>
          </div>
        </Layout>
      </>
    );
  }
}

const mapStates = (state: any) => ({
  performerState: { ...state.performer.performerListing },
  ui: { ...state.ui }
});

const mapDispatch = { getList };
export default connect(mapStates, mapDispatch)(Performers);
