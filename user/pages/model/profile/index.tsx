import { PureComponent } from 'react';
import {
  Layout, Collapse, Tabs, Button, Row,
  Col, message, Modal, Tooltip, Spin
} from 'antd';
import { connect } from 'react-redux';
import {
  getVideos, moreVideo, getVods, moreVod
} from '@redux/video/actions';
import { getGalleries } from '@redux/gallery/actions';
import { listProducts, moreProduct } from '@redux/product/actions';
import { performerService, photoService, paymentService } from 'src/services';
import Head from 'next/head';
import '@components/performer/performer.less';
import {
  CheckCircleOutlined, ArrowDownOutlined, HeartOutlined, ArrowLeftOutlined, EyeOutlined,
  ShopOutlined, VideoCameraOutlined, PictureOutlined, DollarOutlined, UsergroupAddOutlined
} from '@ant-design/icons';
import { ScrollListVideo } from '@components/video';
import { ScrollListProduct } from '@components/product/scroll-list-item';
import { ConfirmSubscriptionPerformerForm, PerformerInfo } from '@components/performer';
import GalleryCard from '@components/gallery/gallery-card';
import PhotosSlider from '@components/photo/photo-slider';
import Link from 'next/link';
import Router from 'next/router';
import { redirectToErrorPage } from '@redux/system/actions';
import Loader from '@components/common/base/loader';
import {
  IPerformer,
  IGallery,
  IUser,
  IUIConfig
} from '../../../src/interfaces';

interface IProps {
  ui: IUIConfig;
  currentUser: IUser | IPerformer;
  performer: IPerformer;
  query: any;
  listProducts: Function;
  getVideos: Function;
  moreVideo: Function;
  getVods: Function;
  moreProduct: Function;
  moreVod: Function;
  videos: any;
  saleVideos: any;
  products: any;
  getGalleries: Function;
  galleries: any;
  error: any;
  redirectToErrorPage: Function;
}
const { Panel } = Collapse;
const { TabPane } = Tabs;

class PerformerProfile extends PureComponent<IProps> {
  static authenticate = true;

  static noredirect = true;

  subscriptionType = 'monthly';

  state = {
    itemPerPage: 24,
    videoPage: 0,
    vodPage: 0,
    productPage: 0,
    sellectedGallery: null,
    galleryPhotos: null,
    visibleModal: false,
    isSubscribed: false,
    viewedVideo: true,
    openSubscriptionModal: false,
    submiting: false
  };

  static async getInitialProps({ ctx }) {
    const { query } = ctx;
    try {
      const performer = (await (
        await performerService.findOne(query.username, {
          Authorization: ctx.token || ''
        })
      ).data) as IPerformer;

      if (!performer) {
        return Router.push('/error/404');
      }

      return {
        performer
      };
    } catch (e) {
      const error = await Promise.resolve(e);
      return { error };
    }
  }

  async componentDidMount() {
    await this.checkBlock();
    const {
      performer
    } = this.props;
    if (performer) {
      this.setState({ isSubscribed: performer.isSubscribed });

      this.loadItems('video');
    }
  }

  async handleIncreaseView() {
    const { performer } = this.props;
    try {
      performerService.increaseView(performer.username);
    } catch (e) {
      // eslint-disable-next-line no-console
      console.log(e);
    }
  }

  async handleShowPhotosSlider(gallery: IGallery, performerId: string) {
    const { isSubscribed } = this.state;
    if (!isSubscribed) {
      return message.error('Subscribe to view full content!');
    }
    try {
      const resp = await (
        await photoService.userSearch(performerId, { galleryId: gallery._id })
      ).data.data;
      return this.setState({
        visibleModal: true,
        sellectedGallery: gallery,
        galleryPhotos: resp
      });
    } catch (e) {
      // console.log(e);
      // TODO - show error
      return undefined;
    }
  }

  handleViewWelcomeVideo() {
    const video = document.getElementById('video') as HTMLVideoElement;
    video.pause();
    this.setState({ viewedVideo: false });
  }

  handleClosePhotosSlider() {
    this.setState({ visibleModal: false });
  }

  loadMoreItem = async (type: string) => {
    const {
      moreVideo: moreVideoHandler,
      moreProduct: moreProductHandler,
      moreVod: moreVodHandler,
      videos: videosVal,
      products: productsVal,
      saleVideos: saleVideosVal,
      performer
    } = this.props;

    const {
      videoPage, itemPerPage, vodPage, productPage
    } = this.state;
    if (type === 'vid') {
      if (videosVal.items.length >= videosVal.total) {
        return false;
      }
      this.setState({
        videoPage: videoPage + 1
      }, () => moreVideoHandler({
        limit: itemPerPage,
        offset: (videoPage + 1) * itemPerPage,
        performerId: performer._id,
        isSaleVideo: false
      }));
    }
    if (type === 'vod') {
      if (saleVideosVal.items.length >= saleVideosVal.total) {
        return false;
      }
      this.setState({
        vodPage: vodPage + 1
      }, () => moreVodHandler({
        limit: itemPerPage,
        offset: (vodPage + 1) * itemPerPage,
        performerId: performer._id,
        isSaleVideo: true
      }));
    }
    if (type === 'product') {
      if (productsVal.items.length >= productsVal.total) {
        return false;
      }
      this.setState({
        productPage: productPage + 1
      }, () => moreProductHandler({
        limit: itemPerPage,
        offset: (productPage + 1) * itemPerPage,
        performerId: performer._id
      }));
    }
    return false;
  };

  loadItems = (type: string) => {
    const { itemPerPage } = this.state;
    const {
      performer,
      getGalleries: getGalleriesHandler,
      listProducts: listProductsHandler,
      getVideos: getVideosHandler,
      getVods: getVodsHandler
    } = this.props;
    switch (type) {
      case 'video':
        this.setState({ videoPage: 0 }, () => {
          getVideosHandler({
            limit: itemPerPage,
            offset: 0,
            performerId: performer._id,
            isSaleVideo: false
          });
        });
        break;
      case 'saleVideo':
        this.setState({ vodPage: 0 }, () => {
          getVodsHandler({
            limit: itemPerPage,
            offset: 0,
            performerId: performer._id,
            isSaleVideo: true
          });
        });
        break;
      case 'photo':
        getGalleriesHandler({
          limit: 200,
          performerId: performer._id
        });
        break;
      case 'store':
        this.setState({ productPage: 0 }, () => {
          listProductsHandler({
            performerId: performer._id,
            limit: itemPerPage,
            offset: 0
          });
        });
        break;
      default: break;
    }
  }

  async checkBlock() {
    const { redirectToErrorPage: redirectToErrorPageHandler, error } = this.props;
    if (error && process.browser) {
      redirectToErrorPageHandler({
        url: '/error',
        error: {
          ...error,
          message:
            // eslint-disable-next-line no-nested-ternary
            error.message === 'BLOCKED_BY_PERFORMER'
              ? 'You have been blocked by this performer, please contact us for any questions'
              : error.message === 'BLOCK_COUNTRY'
                ? 'You cannot view the profile of this model. This model has blocked access from your country'
                : error.message
        }
      });
    }
  }

  async subscribe() {
    const { performer } = this.props;
    try {
      await this.setState({ submiting: true });
      const subscription = await (
        await paymentService.subscribe({ type: this.subscriptionType, performerId: performer._id })
      ).data;
      // throw success now
      if (subscription) {
        message.success('Redirecting to payment gateway.');
        window.location.href = subscription.paymentUrl;
      }
    } catch (e) {
      const err = await e;
      message.error(err.message || 'error occured, please try again later');
    } finally {
      this.setState({ submiting: false });
    }
  }

  render() {
    const {
      performer,
      ui,
      currentUser,
      videos: videoProps,
      products: productProps,
      saleVideos: saleVideoProps,
      galleries: galleryProps
    } = this.props;
    const { items: videos = [], total: totalVideos, requesting: loadingVid } = videoProps;
    const { items: saleVideos = [], total: totalVods, requesting: loadingVod } = saleVideoProps;
    const { items: products, total: totalProducts, requesting: loadingProduct } = productProps;
    const { items: galleries, requesting: loadingGallery } = galleryProps;
    const {
      visibleModal, sellectedGallery, galleryPhotos, viewedVideo, isSubscribed,
      openSubscriptionModal, submiting
    } = this.state;
    return (
      <>
        <Head>
          <title>
            {`${ui?.siteName} | ${performer?.name || performer?.username || ''}`}
          </title>
          <meta
            name="keywords"
            content={`${performer?.username}, ${performer?.name}`}
          />
          <meta name="description" content={performer?.bio} />
          {/* OG tags */}
          <meta
            property="og:title"
            content={`${ui?.siteName} | ${performer?.name || performer?.username || ''}`}
            key="title"
          />
          <meta property="og:image" content={performer?.avatar || '/no-avatar.png'} />
          <meta
            property="og:keywords"
            content={`${performer?.username}, ${performer?.name}`}
          />
          <meta
            property="og:description"
            content={performer?.bio}
          />
        </Head>
        <Layout>
          <div
            className="top-profile"
            style={{
              backgroundImage:
                  performer.cover
                    ? `url('${performer.cover}')`
                    : "url('/banner-image.jpg')"
            }}
          >
            <div className="bg-2nd">
              <div className="main-container">
                <div className="top-banner">
                  <a aria-hidden className="arrow-back" onClick={() => Router.back()}>
                    <ArrowLeftOutlined />
                  </a>
                  <div className="stats-row">
                    <div className="t-user-name">
                      {performer?.name || ''}
                    </div>
                    <div className="tab-stat">
                      <div className="tab-item">
                        <span>
                          {(performer?.stats?.views) || 0}
                          {' '}
                          <EyeOutlined />
                        </span>
                      </div>
                      <div className="tab-item">
                        <span>
                          {(performer?.stats?.totalVideos) || 0}
                          {' '}
                          <VideoCameraOutlined />
                        </span>
                      </div>
                      <div className="tab-item">
                        <span>
                          {(performer?.stats?.totalPhotos) || 0}
                          {' '}
                          <PictureOutlined />
                        </span>
                      </div>
                      <div className="tab-item">
                        <span>
                          {(performer?.stats?.totalProducts) || 0}
                          {' '}
                          <ShopOutlined />
                        </span>
                      </div>
                      <div className="tab-item">
                        <span>
                          {(performer?.stats?.likes) || 0}
                          {' '}
                          <HeartOutlined />
                        </span>
                      </div>
                      <div className="tab-item">
                        <span>
                          {(performer?.stats?.subscribers) || 0}
                          {' '}
                          <UsergroupAddOutlined />
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="main-profile">
            <div className="main-container">
              <div className="fl-col">
                <img
                  alt="Avatar"
                  src={
                    performer?.avatar
                      ? performer.avatar
                      : '/no-avatar.png'
                  }
                />
                <div className="m-user-name">
                  <h4>
                    {performer?.name ? performer.name : ''}
                    &nbsp;
                    <CheckCircleOutlined />
                  </h4>
                  <h5>
                    @
                    {performer?.username}
                  </h5>
                </div>
              </div>
              <div className="btn-grp">
                {/* <button>Send Tip</button> */}
                {currentUser
                  && !currentUser.isPerformer
                  && currentUser._id
                  !== ((performer?._id) || '') && (
                    <button className="normal" type="button">
                      <Link
                        href={{
                          pathname: '/messages',
                          query: {
                            toSource: 'performer',
                            toId: (performer?._id) || ''
                          }
                        }}
                      >
                        <span>Message</span>
                      </Link>
                    </button>
                )}
                {!isSubscribed && performer.yearlyPrice && (
                <Button
                  className="secondary"
                  disabled={submiting && this.subscriptionType === 'yearly'}
                  onClick={() => {
                    if (!currentUser._id) {
                      Router.push('/auth/login');
                      return;
                    }
                    this.subscriptionType = 'yearly';
                    this.setState({ openSubscriptionModal: true });
                  }}
                >
                  Yearly Sub $
                  {performer.yearlyPrice.toFixed(2)}
                </Button>
                )}
                {!isSubscribed && performer.monthlyPrice && (
                <Button
                  type="primary"
                  className="primary"
                  disabled={submiting && this.subscriptionType === 'monthly'}
                  onClick={() => {
                    if (!currentUser._id) {
                      Router.push('/auth/login');
                      return;
                    }
                    this.subscriptionType = 'monthly';
                    this.setState({ openSubscriptionModal: true });
                  }}
                >
                  Monthly Sub $
                  {performer?.monthlyPrice.toFixed(2)}
                </Button>
                )}
              </div>
              <div
                className={
                  currentUser.isPerformer ? 'mar-0 pro-desc' : 'pro-desc'
                }
              >
                <div className="flex-row show-more">
                  <Collapse
                    expandIconPosition="right"
                    bordered={false}
                    expandIcon={({ isActive }) => (
                      <ArrowDownOutlined rotate={isActive ? 180 : 0} />
                    )}
                    className="site-collapse-custom-collapse"
                    defaultActiveKey={['1']}
                  >
                    <Panel
                      header="About me"
                      key="1"
                      className="site-collapse-custom-panel"
                    >
                      <PerformerInfo countries={ui?.countries || []} performer={performer && performer} />
                    </Panel>
                  </Collapse>
                </div>
              </div>
            </div>
          </div>
          <div style={{ marginTop: '20px' }} />
          <div className="main-container">
            <div className="model-content">
              <Tabs defaultActiveKey="Video" size="large" onTabClick={this.loadItems}>
                <TabPane
                  tab={(
                    <Tooltip title="Video">
                      <VideoCameraOutlined />
                    </Tooltip>
                  )}
                  key="video"
                >
                  <ScrollListVideo
                    items={videos}
                    loading={loadingVid}
                    canLoadmore={videos && videos.length < totalVideos}
                    loadMore={this.loadMoreItem.bind(this, 'vid')}
                  />
                </TabPane>
                <TabPane
                  tab={(
                    <Tooltip title="Video on Demand">
                      <DollarOutlined />
                    </Tooltip>
                  )}
                  key="saleVideo"
                >
                  <ScrollListVideo
                    items={saleVideos}
                    loading={loadingVod}
                    canLoadmore={saleVideos && saleVideos.length < totalVods}
                    loadMore={this.loadMoreItem.bind(this, 'vod')}
                  />
                </TabPane>
                <TabPane
                  tab={(
                    <Tooltip title="Gallery">
                      <PictureOutlined />
                    </Tooltip>
                  )}
                  key="photo"
                >
                  {loadingGallery && <div className="text-center"><Spin /></div>}
                  {!loadingGallery && !galleries.length && (
                    <p className="text-center">No data was found</p>
                  )}
                  <Row>
                    {galleries
                      && galleries.length > 0
                      && galleries.map((gallery: IGallery) => (
                        <Col
                          xs={12}
                          sm={12}
                          md={8}
                          lg={6}
                          xl={6}
                          key={gallery._id}
                          onClick={this.handleShowPhotosSlider.bind(
                            this,
                            gallery,
                            performer?._id
                          )}
                        >
                          <GalleryCard gallery={gallery} />
                        </Col>
                      ))}
                  </Row>
                </TabPane>
                <TabPane
                  tab={(
                    <Tooltip title="Store">
                      <ShopOutlined />
                    </Tooltip>
                  )}
                  key="store"
                >
                  <ScrollListProduct
                    items={products}
                    loading={loadingProduct}
                    canLoadmore={products && products.length < totalProducts}
                    loadMore={this.loadMoreItem.bind(this, 'product')}
                  />
                </TabPane>
              </Tabs>
            </div>
          </div>
          {sellectedGallery && (
            <PhotosSlider
              gallery={sellectedGallery}
              photos={galleryPhotos}
              visible={visibleModal}
              subscribed={isSubscribed}
              onClose={this.handleClosePhotosSlider.bind(this)}
            />
          )}
          {performer
            && performer.welcomeVideoPath
            && performer.activateWelcomeVideo && (
              <Modal
                key="welcome-video"
                width={768}
                visible={viewedVideo}
                title="Welcome video"
                onOk={this.handleViewWelcomeVideo.bind(this)}
                onCancel={this.handleViewWelcomeVideo.bind(this)}
                footer={[
                  <Button
                    type="primary"
                    onClick={this.handleViewWelcomeVideo.bind(this)}
                  >
                    Close
                  </Button>
                ]}
              >
                <video
                  autoPlay
                  src={performer.welcomeVideoPath}
                  controls
                  id="video"
                  style={{ width: '100%' }}
                />
              </Modal>
          )}
          <Modal
            key="subscribe_performer"
            title={`Confirm ${this.subscriptionType} subscription ${performer?.name}`}
            visible={openSubscriptionModal}
            confirmLoading={submiting}
            footer={null}
            onCancel={() => this.setState({ openSubscriptionModal: false })}
          >
            <ConfirmSubscriptionPerformerForm
              type={this.subscriptionType || 'monthly'}
              performer={performer}
              submiting={submiting}
              onFinish={this.subscribe.bind(this)}
            />
          </Modal>
          {submiting && <Loader />}
        </Layout>
      </>
    );
  }
}

const mapStates = (state: any) => ({
  ui: state.ui,
  videos: { ...state.video.videos },
  saleVideos: { ...state.video.saleVideos },
  products: { ...state.product.products },
  galleries: { ...state.gallery.listGalleries },
  currentUser: { ...state.user.current }
});

const mapDispatch = {
  getVideos,
  moreVideo,
  getVods,
  listProducts,
  moreProduct,
  getGalleries,
  redirectToErrorPage,
  moreVod
};
export default connect(mapStates, mapDispatch)(PerformerProfile);
