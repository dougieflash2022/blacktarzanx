import { PureComponent } from 'react';
import { Layout, message } from 'antd';
import Head from 'next/head';
import Page from '@components/common/layout/page';
import FormGallery from '@components/gallery/form-gallery';
import { IGalleryCreate, IUIConfig } from 'src/interfaces';
import { galleryService } from 'src/services';
import { getResponseError } from '@lib/utils';
import Router from 'next/router';
import { connect } from 'react-redux';

interface IProps {
  ui: IUIConfig;
}

interface IStates {
  submitting: boolean;
}

class GalleryCreatePage extends PureComponent<IProps, IStates> {
  static authenticate: boolean = true;

  static onlyPerformer: boolean = true;

  static noredirect: boolean = false;

  constructor(props: IProps) {
    super(props);
    this.state = {
      submitting: false
    };
  }

  async onFinish(data: IGalleryCreate) {
    try {
      this.setState({ submitting: true });
      await galleryService.create(data);
      message.success('Gallery have been created.');
    } catch (e) {
      message.error(
        getResponseError(e) || 'An error occurred, please try again!'
      );
    } finally {
      await this.setState({ submitting: true }, () => Router.push('/model/my-gallery/listing'));
    }
  }

  render() {
    const { ui } = this.props;
    const { submitting } = this.state;
    return (
      <Layout>
        <Head>
          <title>
            {' '}
            {ui && ui.siteName}
            {' '}
            | Create Gallery
            {' '}
          </title>
        </Head>
        <div className="main-container">
          {/* <BreadcrumbComponent
              breadcrumbs={[
                { title: 'Gallery Manager', href: '/model/my-gallery/listing' },
                { title: 'New Gallery' }
              ]}
            /> */}
          <Page>
            <div className="page-heading">
              <span>New Gallery</span>
            </div>
            <FormGallery
              submitting={submitting}
              onFinish={this.onFinish.bind(this)}
            />
          </Page>
        </div>
      </Layout>
    );
  }
}

const mapStates = (state: any) => ({
  ui: state.ui
});
export default connect(mapStates)(GalleryCreatePage);
