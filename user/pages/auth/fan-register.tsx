/* eslint-disable prefer-promise-reject-errors */
import {
  Row, Col, Button, Layout, Form, Input, Select
} from 'antd';
import { PureComponent } from 'react';
import Link from 'next/link';
import { registerFan } from '@redux/auth/actions';
import { connect } from 'react-redux';
import Head from 'next/head';
import './index.less';
import { IUIConfig } from 'src/interfaces';

const { Option } = Select;

interface IMemberRegisterProps {
  onRegister: Function;
  submiting: boolean;
}

const MemberRegister = ({ onRegister, submiting }: IMemberRegisterProps) => {
  const onFinish = (values: any) => {
    onRegister(values);
  };

  return (
    <Form
      name="member_register"
      initialValues={{ remember: true, gender: 'male' }}
      onFinish={onFinish}
    >
      <Row>
        <Col xs={12} sm={12} md={12} lg={12}>
          <Form.Item
            name="firstName"
            validateTrigger={['onChange', 'onBlur']}
            rules={[
              { required: true, message: 'Please input your first name!' },
              {
                pattern: new RegExp(
                  /^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/u
                ),
                message:
                  'First name can not contain number and special character'
              }
            ]}
            hasFeedback
          >
            <Input placeholder="First name" />
          </Form.Item>
        </Col>
        <Col xs={12} sm={12} md={12} lg={12}>
          <Form.Item
            name="lastName"
            validateTrigger={['onChange', 'onBlur']}
            rules={[
              { required: true, message: 'Please input your last name!' },
              {
                pattern: new RegExp(
                  /^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/u
                ),
                message:
                  'Last name can not contain number and special character'
              }
            ]}
            hasFeedback
          >
            <Input placeholder="Last name" />
          </Form.Item>
        </Col>
        <Col xs={12} sm={12} md={12} lg={12}>
          <Form.Item
            name="name"
            validateTrigger={['onChange', 'onBlur']}
            rules={[
              { required: true, message: 'Please input your display name!' },
              {
                pattern: new RegExp(/^(?=.*\S).+$/g),
                message:
                  'Display name can not contain only whitespace'
              },
              {
                min: 3,
                message: 'Display name must containt at least 3 characters'
              }
            ]}
            hasFeedback
          >
            <Input placeholder="Display name" />
          </Form.Item>
        </Col>
        <Col xs={12} sm={12} md={12} lg={12}>
          <Form.Item
            name="username"
            validateTrigger={['onChange', 'onBlur']}
            rules={[
              { required: true, message: 'Please input your username!' },
              {
                pattern: new RegExp(/^[A-Za-z0-9]+$/g),
                message:
                  'Username must contain Alphabets & Numbers only'
              },
              { min: 3, message: 'Username must containt at least 3 characters' }
            ]}
            hasFeedback
          >
            <Input placeholder="Username eg: injoker123, miracle9..." />
          </Form.Item>
        </Col>
        <Col xs={12} sm={12} md={12} lg={12}>
          <Form.Item
            name="email"
            validateTrigger={['onChange', 'onBlur']}
            hasFeedback
            rules={[
              {
                type: 'email',
                message: 'Invalid email address!'
              },
              {
                required: true,
                message: 'Please input your email address!'
              }
            ]}
          >
            <Input placeholder="Email address" />
          </Form.Item>
        </Col>
        <Col xs={12} sm={12} md={12} lg={12}>
          <Form.Item
            name="gender"
            validateTrigger={['onChange', 'onBlur']}
            rules={[{ required: true }]}
            hasFeedback
          >
            <Select>
              <Option value="male">Male</Option>
              <Option value="female">Female</Option>
              <Option value="transgender">Transgender</Option>
            </Select>
          </Form.Item>
        </Col>
        <Col xs={24} sm={12} md={12} lg={12}>
          <Form.Item
            name="password"
            validateTrigger={['onChange', 'onBlur']}
            hasFeedback
            rules={[
              { required: true, message: 'Please input your password!' },
              {
                min: 6,
                message: 'Password must contain at least 6 characters.'
              }
            ]}
          >
            <Input type="password" placeholder="Password" />
          </Form.Item>
        </Col>
        <Col xs={24} sm={12} md={12} lg={12}>
          <Form.Item
            name="confirm"
            validateTrigger={['onChange', 'onBlur']}
            dependencies={['password']}
            hasFeedback
            rules={[
              {
                required: true,
                message: 'Please confirm your password!'
              },
              {
                min: 6,
                message: 'Password must contain at least 6 characters.'
              },
              ({ getFieldValue }) => ({
                validator(rule, value) {
                  if (!value || getFieldValue('password') === value) {
                    return Promise.resolve();
                  }
                  return Promise.reject('Passwords do not match together!');
                }
              })
            ]}
          >
            <Input type="password" placeholder="Confirm password" />
          </Form.Item>
        </Col>
      </Row>

      <Form.Item style={{ textAlign: 'center' }}>
        <Button
          type="primary"
          htmlType="submit"
          disabled={submiting}
          loading={submiting}
          style={{
            marginBottom: 15,
            fontWeight: 600,
            padding: '5px 25px',
            height: '42px'
          }}
        >
          Create your Account
        </Button>
        <p>
          Have an account already?
          <Link href="/auth/login">
            <a> Login here.</a>
          </Link>
        </p>
        <p>
          Are you a model?
          <Link href="/auth/model-register">
            <a> Register here.</a>
          </Link>
        </p>
      </Form.Item>
    </Form>
  );
};

interface IProps {
  ui: IUIConfig;
  registerFan: Function;
  registerFanData: any;
}

class FanRegister extends PureComponent<IProps> {
  handleRegister = (data: any) => {
    const { registerFan: handleRegister } = this.props;
    handleRegister(data);
  };

  render() {
    const {
      ui, registerFanData = { requesting: false }
    } = this.props;
    return (
      <Layout>
        <Head>
          <title>
            {ui && ui.siteName}
            {' '}
            | Fans Register
          </title>
        </Head>
        <div className="main-container">
          <div className="login-box register-box">
            <Row>
              <Col
                xs={24}
                sm={24}
                md={6}
                lg={8}
                className="login-content left"
                style={ui.loginPlaceholderImage ? { backgroundImage: `url(${ui.loginPlaceholderImage})` } : null}
              />
              <Col
                xs={24}
                sm={24}
                md={18}
                lg={16}
                className="login-content right"
              >
                <div className="text-center">
                  <span className="title">Fans Register</span>
                </div>
                <p className="text-center"><small>Sign up to interact with your idols!</small></p>
                <div className="register-form">
                  <MemberRegister submiting={registerFanData.requesting} onRegister={this.handleRegister} />
                </div>
              </Col>
            </Row>
          </div>
        </div>
      </Layout>
    );
  }
}
const mapStatesToProps = (state: any) => ({
  ui: { ...state.ui },
  registerFanData: { ...state.auth.registerFanData }
});

const mapDispatchToProps = { registerFan };

export default connect(mapStatesToProps, mapDispatchToProps)(FanRegister);
