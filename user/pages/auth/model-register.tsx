/* eslint-disable prefer-promise-reject-errors */
import {
  Row,
  Col,
  Button,
  Layout,
  Form,
  Input,
  Select,
  message
  // DatePicker
} from 'antd';
import { PureComponent } from 'react';
import Link from 'next/link';
import Head from 'next/head';
import './index.less';
import { connect } from 'react-redux';
import { registerPerformer } from '@redux/auth/actions';
import { IUIConfig } from 'src/interfaces';
import { ImageUpload, FileUpload } from '@components/file';
// import moment from 'moment';

const { Option } = Select;

interface IProps {
  registerPerformerData: any;
  registerPerformer: Function;
  ui: IUIConfig;
}

class RegisterPerformer extends PureComponent<IProps> {
  idVerificationFile = null;

  documentVerificationFile = null;

  state = {
    selectedGender: 'male'
  };

  onFileReaded = (type, file: File) => {
    if (file && type === 'idFile') {
      this.idVerificationFile = file;
    }
    if (file && type === 'documentFile') {
      this.documentVerificationFile = file;
    }
  }

  register = (values: any) => {
    const data = values;
    const { registerPerformer: registerPerformerHandler } = this.props;
    if (!this.idVerificationFile || !this.documentVerificationFile) {
      return message.error('ID photo & ID document are required', 5);
    }
    data.idVerificationFile = this.idVerificationFile;
    data.documentVerificationFile = this.documentVerificationFile;
    return registerPerformerHandler(data);
  };

  render() {
    const { registerPerformerData = { requesting: false }, ui } = this.props;
    const { selectedGender } = this.state;

    const placeholderIdImg = () => {
      switch (selectedGender) {
        case 'male': return '/img-id-man.png';
        case 'female': return '/img-id-woman.png';
        case 'transgender': return '/img-id-man.png';
        case 'couple': return '/img-id-couple.png';
        default: return '/img-id-man.png';
      }
    };

    return (
      <Layout>
        <Head>
          <title>
            {ui && ui.siteName}
            {' '}
            | Model Register
          </title>
        </Head>
        <div className="main-container">
          <div className="login-box register-box">
            <div className="text-center">
              <span className="title">Model Register</span>
            </div>
            <p className="text-center"><small>Sign up to make money and interact with your fans!</small></p>
            <Form
              name="member_register"
              initialValues={{
                remember: true,
                gender: 'male',
                country: 'US'
                // dateOfBirth: moment().subtract(18, 'year').endOf('day')
              }}
              onFinish={this.register}
            >
              <Row>
                <Col
                  xs={24}
                  sm={24}
                  md={14}
                  lg={14}
                >
                  <Row>
                    <Col span={12}>
                      <Form.Item
                        name="firstName"
                        validateTrigger={['onChange', 'onBlur']}
                        rules={[
                          { required: true, message: 'Please input your name!' },
                          {
                            pattern: new RegExp(
                              /^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/u
                            ),
                            message:
                              'First name can not contain number and special character'
                          }
                        ]}
                        hasFeedback
                      >
                        <Input placeholder="First name" />
                      </Form.Item>
                    </Col>
                    <Col span={12}>
                      <Form.Item
                        name="lastName"
                        validateTrigger={['onChange', 'onBlur']}
                        rules={[
                          { required: true, message: 'Please input your name!' },
                          {
                            pattern: new RegExp(
                              /^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/u
                            ),
                            message:
                              'Last name can not contain number and special character'
                          }
                        ]}
                        hasFeedback
                      >
                        <Input placeholder="Last name" />
                      </Form.Item>
                    </Col>
                    <Col span={12}>
                      <Form.Item
                        name="name"
                        validateTrigger={['onChange', 'onBlur']}
                        rules={[
                          { required: true, message: 'Please input your display name!' },
                          {
                            pattern: new RegExp(/^(?=.*\S).+$/g),
                            message:
                              'Display name can not contain only whitespace'
                          },
                          {
                            min: 3,
                            message: 'Display name must containt at least 3 characters'
                          }
                        ]}
                        hasFeedback
                      >
                        <Input placeholder="Display name" />
                      </Form.Item>
                    </Col>
                    <Col span={12}>
                      <Form.Item
                        name="username"
                        validateTrigger={['onChange', 'onBlur']}
                        rules={[
                          { required: true, message: 'Please input your username!' },
                          {
                            pattern: new RegExp(/^[a-zA-Z0-9]+$/g),
                            message:
                              'Username must contain only Alphabets & Numbers'
                          },
                          { min: 3, message: 'username must containt at least 3 characters' }
                        ]}
                        hasFeedback
                      >
                        <Input placeholder="Username eg: user123, chirst99 ..." />
                      </Form.Item>
                    </Col>
                    <Col span={24}>
                      <Form.Item
                        name="email"
                        validateTrigger={['onChange', 'onBlur']}
                        hasFeedback
                        rules={[
                          {
                            type: 'email',
                            message: 'The input is not valid E-mail!'
                          },
                          {
                            min: 6,
                            message: 'Password must contain at least 6 characters.'
                          },
                          {
                            required: true,
                            message: 'Please input your E-mail!'
                          }
                        ]}
                      >
                        <Input placeholder="Email address" />
                      </Form.Item>
                    </Col>
                    {/* <Col span={12}>
                      <Form.Item
                        name="dateOfBirth"
                        validateTrigger={['onChange', 'onBlur']}
                        hasFeedback
                        rules={[
                          {
                            required: true,
                            message: 'Select your date of birth'
                          }
                        ]}
                      >
                        <DatePicker
                          disabledDate={(currentDate) => currentDate && currentDate > moment().subtract(14, 'year').endOf('day')}
                        />
                      </Form.Item>
                    </Col> */}
                    <Col span={12}>
                      <Form.Item name="country" rules={[{ required: true }]} hasFeedback>
                        <Select
                          showSearch
                          filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                        >
                          {ui.countries
                            && ui.countries.length > 0
                            && ui.countries.map((c) => (
                              <Option value={c.code} key={c.code}>
                                <img alt="country_flag" src={c.flag} width="25px" />
                                {' '}
                                {c.name}
                              </Option>
                            ))}
                        </Select>
                      </Form.Item>
                    </Col>
                    <Col span={12}>
                      <Form.Item
                        name="gender"
                        validateTrigger={['onChange', 'onBlur']}
                        rules={[{ required: true, message: 'Please select your orientation' }]}
                        hasFeedback
                      >
                        <Select onChange={(val) => this.setState({ selectedGender: val })}>
                          <Option value="male" key="male">Male</Option>
                          <Option value="female" key="female">Female</Option>
                          {/* <Option value="couple" key="couple">Couple</Option> */}
                          <Option value="transgender" key="trans">Trans</Option>
                        </Select>
                      </Form.Item>
                    </Col>
                    <Col span={12}>
                      <Form.Item
                        name="password"
                        hasFeedback
                        rules={[
                          { required: true, message: 'Please input your Password!' },
                          {
                            min: 6,
                            message: 'Password must contain at least 6 characters.'
                          }
                        ]}
                      >
                        <Input type="password" placeholder="Password" />
                      </Form.Item>
                    </Col>
                    <Col span={12}>
                      <Form.Item
                        name="confirm"
                        dependencies={['password']}
                        hasFeedback
                        validateTrigger={['onChange', 'onBlur']}
                        rules={[
                          {
                            required: true,
                            message: 'Please enter confirm password!'
                          },
                          ({ getFieldValue }) => ({
                            validator(rule, value) {
                              if (!value || getFieldValue('password') === value) {
                                return Promise.resolve();
                              }
                              return Promise.reject('Passwords do not match together!');
                            }
                          })
                        ]}
                      >
                        <Input type="password" placeholder="Confirm password" />
                      </Form.Item>
                    </Col>
                  </Row>
                </Col>
                <Col
                  xs={24}
                  sm={24}
                  md={10}
                  lg={10}
                >
                  <div className="register-form">
                    <Form.Item
                      labelCol={{ span: 24 }}
                      name="idVerificationId"
                      className="model-photo-verification"
                      help="Upload a photo of yourself holding your indentity document next to your face"
                    >
                      <div className="id-block">
                        <ImageUpload onFileReaded={this.onFileReaded.bind(this, 'idFile')} />
                        <img alt="identity-img" className="img-id" src={placeholderIdImg()} />
                      </div>
                    </Form.Item>
                    <Form.Item
                      labelCol={{ span: 24 }}
                      name="documentVerificationId"
                      className="model-photo-verification"
                      help="Please upload proof of one of either of the following: social security number or national insurance number or passport or a different photographic id to your photo verification (image,pdf,doc,zip...)"
                    >
                      <div className="id-block">
                        <FileUpload onFileReaded={this.onFileReaded.bind(this, 'documentFile')} accept="image/*,.pdf,.doc,.docx, .zip, .rar" />
                        <img alt="identity-img" className="img-id" src="/id-document.png" />
                      </div>
                    </Form.Item>
                  </div>
                </Col>
              </Row>
              <Form.Item style={{ textAlign: 'center' }}>
                <Button
                  type="primary"
                  htmlType="submit"
                  disabled={registerPerformerData.requesting}
                  loading={registerPerformerData.requesting}
                  style={{
                    marginBottom: 15,
                    fontWeight: 600,
                    padding: '5px 25px',
                    height: '42px'
                  }}
                >
                  Create your Account
                </Button>
                <p>
                  Have an account already?
                  <Link href="/auth/login">
                    <a> Login here.</a>
                  </Link>
                </p>
                <p>
                  Are you a fan?
                  <Link href="/auth/fan-register">
                    <a> Register here.</a>
                  </Link>
                </p>
              </Form.Item>
            </Form>
          </div>
        </div>
      </Layout>
    );
  }
}

const mapStatesToProps = (state: any) => ({
  ui: { ...state.ui },
  registerPerformerData: { ...state.auth.registerPerformerData }
});

const mapDispatchToProps = { registerPerformer };

export default connect(mapStatesToProps, mapDispatchToProps)(RegisterPerformer);
