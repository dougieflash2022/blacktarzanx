/* eslint-disable no-console */
/* eslint-disable react/no-unescaped-entities */
import {
  Form,
  Checkbox,
  Input,
  Button,
  Row,
  Col,
  Divider,
  Layout
} from 'antd';
import { PureComponent } from 'react';
import { connect } from 'react-redux';
import Head from 'next/head';
import {
  login, loginPerformer, loginSuccess
} from '@redux/auth/actions';
import { updateCurrentUser } from '@redux/user/actions';
import { authService, performerService, userService } from '@services/index';
import Link from 'next/link';
import './index.less';
import { IUIConfig } from 'src/interfaces';
import Router from 'next/router';
import Loader from '@components/common/base/loader';
import { isEmail } from '@lib/string';

interface IProps {
  loginAuth: any;
  login: Function;
  loginPerformer: Function;
  updateCurrentUser: Function;
  loginSuccess: Function;
  ui: IUIConfig
}

class Login extends PureComponent<IProps> {
  recaptchaSuccess = false;

  state = {
    loginAs: 'user',
    isLoading: false,
    loginInput: ''
  }

  async componentDidMount() {
    this.redirectLogin();
  }

  handleSwitch(value: any) {
    this.setState({ loginAs: value });
  }

  async handleLogin(values: any) {
    const { loginPerformer: handleLoginPerformer, login: handleLogin } = this.props;
    const { loginAs, loginInput } = this.state;
    const data = values;
    const isInputEmail = isEmail(loginInput);
    data.loginUsername = !isInputEmail;
    if (isInputEmail) {
      data.email = loginInput;
    } else {
      data.username = loginInput;
    }
    if (loginAs !== 'user') {
      return handleLoginPerformer(data);
    }
    return handleLogin(data);
  }

  onInputChange(e) {
    if (!e.target.value) return;
    this.setState({ loginInput: e.target.value });
  }

  async redirectLogin() {
    const { loginSuccess: handleLogin, updateCurrentUser: handleUpdateUser } = this.props;
    const token = authService.getToken();
    const role = authService.getUserRole();
    if (!token || token === 'null') {
      return;
    }
    authService.setToken(token, role || 'user');
    let user = null;
    try {
      if (role === 'performer') {
        user = await performerService.me({
          Authorization: token
        });
      } else {
        user = await userService.me({
          Authorization: token
        });
      }
      // TODO - check permission
      if (!user.data._id) {
        return;
      }
      handleLogin();
      handleUpdateUser(user.data);
      Router.replace('/home');
    } catch (e) {
      console.log(await e);
    }
  }

  render() {
    const { ui } = this.props;
    const {
      loginAs, isLoading
    } = this.state;
    return (
      <>
        <Head>
          <title>
            {ui && ui.siteName}
            {' '}
            | Login
          </title>
        </Head>
        <Layout>
          <div className="main-container">
            <div className="login-box">
              <Row>
                <Col
                  xs={24}
                  sm={24}
                  md={6}
                  lg={12}
                  className="login-content left fixed"
                  style={ui.loginPlaceholderImage ? { backgroundImage: `url(${ui.loginPlaceholderImage})` } : null}
                />
                <Col
                  xs={24}
                  sm={24}
                  md={18}
                  lg={12}
                  className="login-content right"
                >
                  <p className="text-center"><small>{loginAs === 'performer' ? 'Sign up to make money and interact with your fans!' : 'Sign up to interact with your idols!'}</small></p>
                  <div className="switch-btn">
                    <button type="button" className={loginAs === 'user' ? 'active' : ''} onClick={this.handleSwitch.bind(this, 'user')} style={{ marginRight: '20px' }}>Fans Login</button>
                    <button type="button" className={loginAs === 'performer' ? 'active' : ''} onClick={this.handleSwitch.bind(this, 'performer')}>Model Login</button>
                  </div>
                  <Divider>*</Divider>
                  <div className="login-form">
                    <Form
                      name={`normal_login${loginAs === 'user' ? '1' : 0}`}
                      className="login-form"
                      initialValues={{ remember: true }}
                      onFinish={this.handleLogin.bind(this)}
                    >
                      <Form.Item
                        name="email"
                        hasFeedback
                        validateTrigger={['onChange', 'onBlur']}
                        rules={[
                          { required: true, message: 'Email or Username is missing' }
                        ]}
                      >
                        <Input onChange={this.onInputChange.bind(this)} placeholder="Email address or Username" />
                      </Form.Item>
                      <Form.Item
                        name="password"
                        hasFeedback
                        validateTrigger={['onChange', 'onBlur']}
                        rules={[
                          { required: true, message: 'Please enter your password!' },
                          { min: 6, message: 'Password is at least 6 characters' }
                        ]}
                      >
                        <Input type="password" placeholder="Password" />
                      </Form.Item>
                      <Form.Item>
                        <Row>
                          <Col span={12}>
                            <Form.Item name="remember" valuePropName="checked" noStyle>
                              <Checkbox>Remember me</Checkbox>
                            </Form.Item>
                          </Col>
                          <Col span={12} style={{ textAlign: 'right' }}>
                            <Link
                              href={{
                                pathname: '/auth/forgot-password',
                                query: { type: loginAs !== 'user' ? 'performer' : 'user' }
                              }}
                            >
                              <a className="login-form-forgot">Forgot password?</a>
                            </Link>
                          </Col>
                        </Row>
                      </Form.Item>
                      <Form.Item style={{ textAlign: 'center' }}>
                        <Button type="primary" htmlType="submit" className="login-form-button">
                          Login
                        </Button>
                        <p>
                          Don't have an account yet ?
                          <Link
                            href={
                              loginAs !== 'user' ? '/auth/model-register' : '/auth/fan-register'
                            }
                          >
                            <a> Create an account</a>
                          </Link>
                        </p>
                      </Form.Item>
                    </Form>
                  </div>
                </Col>
              </Row>
              {isLoading && <Loader />}
            </div>
          </div>
        </Layout>
      </>
    );
  }
}

const mapStatesToProps = (state: any) => ({
  ui: { ...state.ui }
});

const mapDispatchToProps = {
  login, loginPerformer, loginSuccess, updateCurrentUser
};
export default connect(mapStatesToProps, mapDispatchToProps)(Login) as any;
