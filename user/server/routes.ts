// import Routes from 'next-routes';
const routes = require('next-routes');

/**
 * routes.add([name], pattern = /name, page = name)
   routes.add(object)
 */

export default routes()
  .add('top-models', '/top-models', '/model/top-models')
  .add('account', '/model/account', '/model/account')
  .add('my-gallery', '/model/my-gallery', '/model/my-gallery')
  .add('my-order', '/model/my-order', '/model/my-order')
  .add('my-photo', '/model/my-photo', '/model/my-photo')
  .add('store-manager', '/model/my-store', '/model/my-store')
  .add('video-manager', '/model/my-video', '/model/my-video')
  .add('my-subscriber', '/model/my-subscriber', '/model/my-subscriber')
  .add('earning', '/model/earning', '/model/earning')
  .add('model', '/model/:username', '/model/profile')
  .add('video', '/video/:id', '/video')
  .add('store', '/store/:id', '/store')
  .add('page', '/page/:id', '/page');
